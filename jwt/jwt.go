package jwt

import (
	"errors"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"gitlab.com/polify/polify_api/logrz"
)

var log = logrz.LL

// Wrapper wraps the signing key and the issuer
type Wrapper struct {
	SecretKey       string
	Issuer          string
	ExpirationHours int64
}

// Claim adds username and userid as a claim to the token
type Claim struct {
	UserName string
	UserID   string
	jwt.StandardClaims
}

var (
	ErrExpiredToken = errors.New("expired token")
	ErrBadClaims    = errors.New("bad claims")
	ErrInvalidToken = errors.New("bad token")
	ErrInvalidAlg   = errors.New("invalid alg")
)

// GenerateToken generates a jwt token
func (w *Wrapper) GenerateToken(name, id string) (signedToken string, err error) {
	var token *jwt.Token

	claims := &Claim{
		UserName: name,
		UserID:   id,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Local().Add(
				time.Hour * time.Duration(w.ExpirationHours),
			).Unix(),
			Issuer: w.Issuer,
		},
	}

	token = jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	if signedToken, err = token.SignedString([]byte(w.SecretKey)); err != nil {
		return
	}

	return
}

// ValidateToken validates the jwt token
func (w *Wrapper) ValidateToken(signedToken string) (claims *Claim, err error) {
	var ok bool
	var token *jwt.Token

	if token, err = jwt.ParseWithClaims(
		signedToken,
		&Claim{},
		func(token *jwt.Token) (interface{}, error) {
			if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, ErrInvalidAlg
			}
			return []byte(w.SecretKey), nil
		},
	); err != nil {
		log.Errorf(" Bad token : %s\n", err.Error())
		err = ErrInvalidToken
		return
	}

	if claims, ok = token.Claims.(*Claim); !ok {
		err = ErrBadClaims
		log.Errorf(" Bad token : %s\n", err.Error())
		return
	}

	if claims.ExpiresAt < time.Now().Local().Unix() {
		err = ErrExpiredToken
		// log.Errorf(" Bad token : %s\n", err.Error())
		return
	}

	return
}
