package bucket

import (
	"context"
	"errors"
	"fmt"
	"net/url"
	"time"

	"gitlab.com/polify/polify_api/db"
	"gitlab.com/polify/polify_api/graph/model"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

const (
	DEFAULT_CLIENT_LOCATION = "us-east-1"
	DEFAULT_BUCKET_REGION   = "eu-central-1"
	DEFAULT_BUCKET_HOST     = "wasabisys.com"
)

// Bucket ...
type Bucket struct {
	Name   string
	Client *minio.Client
}

func NewBucket(endpoint, bucketname, accessKeyID, accessKeySecret string, clientRegion *string) (*Bucket, error) {
	var err error
	var useSSL = true
	var clientLocation = DEFAULT_CLIENT_LOCATION
	var nBucket = Bucket{
		Name: bucketname,
	}
	if clientRegion != nil {
		clientLocation = *clientRegion
	}
	// log.Printf("Will try to connect to bucket at %s\n", endpoint)
	if nBucket.Client, err = minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, accessKeySecret, ""),
		Secure: useSSL,
		Region: clientLocation,
	}); err != nil {
		return nil, err
	}
	return &nBucket, nil
}

func GetUserBucketClient(cli *db.PrismaClient, userId string) (*Bucket, error) {
	var (
		err          error
		ctx          = context.Background()
		s3Credential *db.S3CredentialModel
		bucketClient *Bucket
		bucketRegion = DEFAULT_BUCKET_REGION
		bucketHost   = DEFAULT_BUCKET_HOST
	)
	if s3Credential, err = cli.S3Credential.FindUnique(
		db.S3Credential.OwnerID.Equals(userId),
	).Exec(ctx); err != nil {
		return nil, errors.New("internal system error")
	}

	if val, ok := s3Credential.BucketRegion(); val != "" && ok {
		bucketRegion = val

	}
	if val := s3Credential.BucketHost; val != "" {
		bucketHost = s3Credential.BucketHost
	}
	if bucketClient, err = NewBucket(
		fmt.Sprintf("s3.%s.%s", bucketRegion, bucketHost),
		s3Credential.BucketName,
		s3Credential.KeyID,
		s3Credential.KeySecret,
		nil,
		// s3Credential.ClientRegion,
	); err != nil {
		return nil, fmt.Errorf("internal system error")
	}
	return bucketClient, nil
}

func GetSignedUrl(cli *db.PrismaClient, song *db.SongModel, userId string) (*model.S3SignedURL, error) {
	var (
		err          error
		ctx          = context.Background()
		s3Credential *db.S3CredentialModel
		bucketClient *Bucket
		bucketRegion = DEFAULT_BUCKET_REGION
		bucketHost   = DEFAULT_BUCKET_HOST
		signedURL    *url.URL
	)
	if s3Credential, err = cli.S3Credential.FindUnique(
		db.S3Credential.OwnerID.Equals(userId),
	).Exec(ctx); err != nil {
		return nil, errors.New("internal system error")
	}

	if val, ok := s3Credential.BucketRegion(); val != "" && ok {
		bucketRegion = val

	}
	if val := s3Credential.BucketHost; val != "" {
		bucketHost = s3Credential.BucketHost
	}

	if bucketClient, err = NewBucket(
		fmt.Sprintf("s3.%s.%s", bucketRegion, bucketHost),
		s3Credential.BucketName,
		s3Credential.KeyID,
		s3Credential.KeySecret,
		nil,
		// s3Credential.ClientRegion,
	); err != nil {
		return nil, fmt.Errorf("internal system error")
	}
	if signedURL, err = bucketClient.Client.PresignedGetObject(
		ctx,
		s3Credential.BucketName,
		song.S3Key,
		72*time.Hour,
		nil,
	); err != nil {
		return nil, fmt.Errorf("internal system error")
	}
	return &model.S3SignedURL{
		SignedURL: signedURL.String(),
		ExpireAt:  time.Now().Local().Add(72 * time.Hour),
	}, nil
}
