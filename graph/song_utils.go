package graph

import (
	"context"
	"time"

	"gitlab.com/polify/polify_api/db"
	"gitlab.com/polify/polify_api/graph/model"

	"gitlab.com/polify/polify_api/bucket"
)

const PGTIMESTAMPFORMAT = time.RFC3339

func GetSongsModel(cli *db.PrismaClient, ss []db.SongModel, withSignedUrl bool) ([]*model.Song, error) {
	var (
		songs = []*model.Song{}
		ctx   = context.Background()
	)
	for i := range ss {
		nSong := &model.Song{
			ID:        ss[i].ID,
			Title:     ss[i].Title,
			S3Key:     ss[i].S3Key,
			Duration:  ss[i].Duration,
			CreatedAt: ss[i].CreatedAt,
			UpdatedAt: ss[i].UpdatedAt,
		}
		if v, ok := ss[i].CoverURL(); ok {
			nSong.CoverURL = &v
		}
		genres := ss[i].Genres()
		for ind := range genres {
			nSong.Genres = append(nSong.Genres, &model.MusicGenre{
				ID:   genres[ind].ID,
				Name: genres[ind].Name,
				Type: string(genres[ind].Type),
			})
		}
		if v, ok := ss[i].Album(); ok {
			nSong.Album = &model.Album{
				ID:        v.ID,
				Name:      v.Name,
				S3Key:     v.S3Key,
				CreatedAt: v.CreatedAt,
				UpdatedAt: v.UpdatedAt,
			}
			if p, ok := v.CoverURL(); ok {
				nSong.Album.CoverURL = &p
			}
		}
		if v, ok := ss[i].Artist(); ok {
			nSong.Artist = &model.Artist{
				ID:        v.ID,
				Name:      v.Name,
				CreatedAt: v.CreatedAt,
				UpdatedAt: v.UpdatedAt,
			}
			if v, ok := v.CoverURL(); ok {
				nSong.Artist.CoverURL = &v
			}
		}
		if sURL, ok := ss[i].S3SignedURL(); ok && withSignedUrl {
			nSong.SignedURL = &model.S3SignedURL{
				ID:        sURL.ID,
				CreatedAt: sURL.CreatedAt,
				UpdatedAt: sURL.UpdatedAt,
				ExpireAt:  sURL.ExpireAt,
				SignedURL: sURL.URL,
			}
			if nSong.SignedURL.ExpireAt.Before(time.Now().Local()) {
				cSigned, err := bucket.GetSignedUrl(cli, &ss[i], ss[i].OwnerID)
				if cSigned != nil {
					var updatedSignedUrl *db.S3SignedURLModel
					if updatedSignedUrl, err = cli.S3SignedURL.UpsertOne(
						db.S3SignedURL.ID.Equals(nSong.SignedURL.ID),
					).Create(
						db.S3SignedURL.URL.SetIfPresent(&cSigned.SignedURL),
						db.S3SignedURL.ExpireAt.Set(cSigned.ExpireAt),
					).Update(
						db.S3SignedURL.URL.SetIfPresent(&cSigned.SignedURL),
						db.S3SignedURL.ExpireAt.Set(cSigned.ExpireAt),
					).Exec(ctx); err != nil {
						log.Errorf("Failed to update song id '%s' : %s\n", nSong.ID, err.Error())
						return nil, err
					}

					nSong.SignedURL = cSigned
					nSong.SignedURL.ID = updatedSignedUrl.ID
					nSong.SignedURL.SignedURL = updatedSignedUrl.URL
					nSong.SignedURL.CreatedAt = updatedSignedUrl.CreatedAt
					nSong.SignedURL.UpdatedAt = updatedSignedUrl.UpdatedAt
				} else if err != nil {
					log.Errorf("failed to get an updated signedUrl : %s\n", err.Error())
					return nil, err
				}
			}
		} else if withSignedUrl {
			cSigned, err := bucket.GetSignedUrl(cli, &ss[i], ss[i].OwnerID)
			if cSigned != nil {
				var nSignedUrl *db.S3SignedURLModel
				if nSignedUrl, err = cli.S3SignedURL.CreateOne(
					db.S3SignedURL.URL.Set(cSigned.SignedURL),
					db.S3SignedURL.ExpireAt.Set(cSigned.ExpireAt),
					db.S3SignedURL.Song.Link(db.Song.ID.Equals(ss[i].ID)),
				).Exec(ctx); err != nil {
					log.Errorf("Failed to create signed url : %s\n", err.Error())
					return nil, err
				}
				nSong.SignedURL = cSigned
				nSong.SignedURL.ID = nSignedUrl.ID
				nSong.SignedURL.SignedURL = nSignedUrl.URL
				nSong.SignedURL.CreatedAt = nSignedUrl.CreatedAt
				nSong.SignedURL.UpdatedAt = nSignedUrl.UpdatedAt
			} else if err != nil {
				log.Errorf("failed to get signed url : %s\n", err.Error())
				return nil, err
			}
		}
		songs = append(songs, nSong)
	}
	return songs, nil
}

func GetSharedAccessUserIDs(userID string, cli *db.PrismaClient) ([]string, error) {
	var sharedFromIds = []string{userID}
	ss, err := cli.SharingRequest.FindMany(
		db.SharingRequest.FromUserID.Equals(userID),
		db.SharingRequest.And(
			db.SharingRequest.Accepted.Equals(true),
		),
	).
		Exec(context.Background())
	if err != nil && err != db.ErrNotFound {
		return nil, err
	}
	for el := range ss {
		sharedFromIds = append(sharedFromIds, ss[el].ToUserID)
	}
	return sharedFromIds, nil
}
