package graph

import (
	"gitlab.com/polify/polify_api/db"

	"github.com/go-redis/redis/v8"
)

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	DB    *db.PrismaClient
	Cache *redis.Client
}
