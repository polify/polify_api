package model

import "gitlab.com/polify/polify_api/db"

/* ID3Genres reference music genres as defined by ID3.org https://id3.org/id3v2.3.0
0. Blues
1. Classic Rock
2. Country
3. Dance
4. Disco
5. Funk
6. Grunge
7. Hip-Hop
8. Jazz
9. Metal
10. New Age
11. Oldies
12. Other
13. Pop
14. R&B
15. Rap
16. Reggae
17. Rock
18. Techno
19. Industrial
20. Alternative
21. Ska
22. Death Metal
23. Pranks
24. Soundtrack
25. Euro-Techno
26. Ambient
27. Trip-Hop
28. Vocal
29. Jazz+Funk
30. Fusion
31. Trance
32. Classical
33. Instrumental
34. Acid
35. House
36. Game
37. Sound Clip
38. Gospel
39. Noise
40. AlternRock
41. Bass
42. Soul
43. Punk
44. Space
45. Meditative
46. Instrumental Pop
47. Instrumental Rock
48. Ethnic
49. Gothic
50. Darkwave
51. Techno-Industrial
52. Electronic
53. Pop-Folk
54. Eurodance
55. Dream
56. Southern Rock
57. Comedy
58. Cult
59. Gangsta
60. Top 40
61. Christian Rap
62. Pop/Funk
63. Jungle
64. Native American
65. Cabaret
66. New Wave
67. Psychadelic
68. Rave
69. Showtunes
70. Trailer
71. Lo-Fi
72. Tribal
73. Acid Punk
74. Acid Jazz
75. Polka
76. Retro
77. Musical
78. Rock & Roll
79. Hard Rock
*/
var ID3Genres = []MusicGenre{
	{Name: "Blues", Type: string(db.MusicGenreTypeID3)},
	{Name: "Classic Rock", Type: string(db.MusicGenreTypeID3)},
	{Name: "Country", Type: string(db.MusicGenreTypeID3)},
	{Name: "Dance", Type: string(db.MusicGenreTypeID3)},
	{Name: "Disco", Type: string(db.MusicGenreTypeID3)},
	{Name: "Funk", Type: string(db.MusicGenreTypeID3)},
	{Name: "Grunge", Type: string(db.MusicGenreTypeID3)},
	{Name: "Hip-Hop", Type: string(db.MusicGenreTypeID3)},
	{Name: "Jazz", Type: string(db.MusicGenreTypeID3)},
	{Name: "Metal", Type: string(db.MusicGenreTypeID3)},
	{Name: "New Age", Type: string(db.MusicGenreTypeID3)},
	{Name: "Oldies", Type: string(db.MusicGenreTypeID3)},
	{Name: "Other", Type: string(db.MusicGenreTypeID3)},
	{Name: "Pop", Type: string(db.MusicGenreTypeID3)},
	{Name: "R&B", Type: string(db.MusicGenreTypeID3)},
	{Name: "Rap", Type: string(db.MusicGenreTypeID3)},
	{Name: "Reggae", Type: string(db.MusicGenreTypeID3)},
	{Name: "Rock", Type: string(db.MusicGenreTypeID3)},
	{Name: "Techno", Type: string(db.MusicGenreTypeID3)},
	{Name: "Industrial", Type: string(db.MusicGenreTypeID3)},
	{Name: "Alternative", Type: string(db.MusicGenreTypeID3)},
	{Name: "Ska", Type: string(db.MusicGenreTypeID3)},
	{Name: "Death Metal", Type: string(db.MusicGenreTypeID3)},
	{Name: "Pranks", Type: string(db.MusicGenreTypeID3)},
	{Name: "Soundtrack", Type: string(db.MusicGenreTypeID3)},
	{Name: "Euro-Techno", Type: string(db.MusicGenreTypeID3)},
	{Name: "Ambient", Type: string(db.MusicGenreTypeID3)},
	{Name: "Trip-Hop", Type: string(db.MusicGenreTypeID3)},
	{Name: "Vocal", Type: string(db.MusicGenreTypeID3)},
	{Name: "Jazz+Funk", Type: string(db.MusicGenreTypeID3)},
	{Name: "Fusion", Type: string(db.MusicGenreTypeID3)},
	{Name: "Trance", Type: string(db.MusicGenreTypeID3)},
	{Name: "Classical", Type: string(db.MusicGenreTypeID3)},
	{Name: "Instrumental", Type: string(db.MusicGenreTypeID3)},
	{Name: "Acid", Type: string(db.MusicGenreTypeID3)},
	{Name: "House", Type: string(db.MusicGenreTypeID3)},
	{Name: "Game", Type: string(db.MusicGenreTypeID3)},
	{Name: "Sound Clip", Type: string(db.MusicGenreTypeID3)},
	{Name: "Gospel", Type: string(db.MusicGenreTypeID3)},
	{Name: "Noise", Type: string(db.MusicGenreTypeID3)},
	{Name: "AlternRock", Type: string(db.MusicGenreTypeID3)},
	{Name: "Bass", Type: string(db.MusicGenreTypeID3)},
	{Name: "Soul", Type: string(db.MusicGenreTypeID3)},
	{Name: "Punk", Type: string(db.MusicGenreTypeID3)},
	{Name: "Space", Type: string(db.MusicGenreTypeID3)},
	{Name: "Meditative", Type: string(db.MusicGenreTypeID3)},
	{Name: "Instrumental Pop", Type: string(db.MusicGenreTypeID3)},
	{Name: "Instrumental Rock", Type: string(db.MusicGenreTypeID3)},
	{Name: "Ethnic", Type: string(db.MusicGenreTypeID3)},
	{Name: "Gothic", Type: string(db.MusicGenreTypeID3)},
	{Name: "Darkwave", Type: string(db.MusicGenreTypeID3)},
	{Name: "Techno-Industrial", Type: string(db.MusicGenreTypeID3)},
	{Name: "Electronic", Type: string(db.MusicGenreTypeID3)},
	{Name: "Pop-Folk", Type: string(db.MusicGenreTypeID3)},
	{Name: "Eurodance", Type: string(db.MusicGenreTypeID3)},
	{Name: "Dream", Type: string(db.MusicGenreTypeID3)},
	{Name: "Southern Rock", Type: string(db.MusicGenreTypeID3)},
	{Name: "Comedy", Type: string(db.MusicGenreTypeID3)},
	{Name: "Cult", Type: string(db.MusicGenreTypeID3)},
	{Name: "Gangsta", Type: string(db.MusicGenreTypeID3)},
	{Name: "Top 40", Type: string(db.MusicGenreTypeID3)},
	{Name: "Christian Rap", Type: string(db.MusicGenreTypeID3)},
	{Name: "Pop/Funk", Type: string(db.MusicGenreTypeID3)},
	{Name: "Jungle", Type: string(db.MusicGenreTypeID3)},
	{Name: "Native American", Type: string(db.MusicGenreTypeID3)},
	{Name: "Cabaret", Type: string(db.MusicGenreTypeID3)},
	{Name: "New Wave", Type: string(db.MusicGenreTypeID3)},
	{Name: "Psychadelic", Type: string(db.MusicGenreTypeID3)},
	{Name: "Rave", Type: string(db.MusicGenreTypeID3)},
	{Name: "Showtunes", Type: string(db.MusicGenreTypeID3)},
	{Name: "Trailer", Type: string(db.MusicGenreTypeID3)},
	{Name: "Lo-Fi", Type: string(db.MusicGenreTypeID3)},
	{Name: "Tribal", Type: string(db.MusicGenreTypeID3)},
	{Name: "Acid Punk", Type: string(db.MusicGenreTypeID3)},
	{Name: "Acid Jazz", Type: string(db.MusicGenreTypeID3)},
	{Name: "Polka", Type: string(db.MusicGenreTypeID3)},
	{Name: "Retro", Type: string(db.MusicGenreTypeID3)},
	{Name: "Musical", Type: string(db.MusicGenreTypeID3)},
	{Name: "Rock & Roll", Type: string(db.MusicGenreTypeID3)},
	{Name: "Hard Rock", Type: string(db.MusicGenreTypeID3)},
}

/* WinampExtension reference music genres as defined by ID3.org in Winamp extension paragraf https://id3.org/id3v2.3.0
 80. Folk
 81. Folk-Rock
 82. National Folk
 83. Swing
 84. Fast Fusion
 85. Bebob
 86. Latin
 87. Revival
 88. Celtic
 89. Bluegrass
 90. Avantgarde
 91. Gothic Rock
 92. Progressive Rock
 93. Psychedelic Rock
 94. Symphonic Rock
 95. Slow Rock
 96. Big Band
 97. Chorus
 98. Easy Listening
 99. Acoustic
100. Humour
101. Speech
102. Chanson
103. Opera
104. Chamber Music
105. Sonata
106. Symphony
107. Booty Bass
108. Primus
109. Porn Groove
110. Satire
111. Slow Jam
112. Club
113. Tango
114. Samba
115. Folklore
116. Ballad
117. Power Ballad
118. Rhythmic Soul
119. Freestyle
120. Duet
121. Punk Rock
122. Drum Solo
123. A cappella
124. Euro-House
125. Dance Hall
*/
var WinampExtension = []MusicGenre{
	{Name: "Folk", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Folk-Rock", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "National Folk", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Swing", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Fast Fusion", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Bebob", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Latin", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Revival", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Celtic", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Bluegrass", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Avantgarde", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Gothic Rock", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Progressive Rock", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Psychedelic Rock", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Symphonic Rock", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Slow Rock", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Big Band", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Chorus", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Easy Listening", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Acoustic", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Humour", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Speech", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Chanson", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Opera", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Chamber Music", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Sonata", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Symphony", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Booty Bass", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Primus", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Porn Groove", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Satire", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Slow Jam", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Club", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Tango", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Samba", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Folklore", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Ballad", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Power Ballad", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Rhythmic Soul", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Freestyle", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Duet", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Punk Rock", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Drum Solo", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "A cappella", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Euro-House", Type: string(db.MusicGenreTypeWINAMP)},
	{Name: "Dance Hall", Type: string(db.MusicGenreTypeWINAMP)},
}
