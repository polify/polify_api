// Code generated by github.com/99designs/gqlgen, DO NOT EDIT.

package model

import (
	"time"
)

type Album struct {
	ID        string        `json:"id"`
	CreatedAt time.Time     `json:"createdAt"`
	UpdatedAt time.Time     `json:"updatedAt"`
	Name      string        `json:"name"`
	S3Key     string        `json:"s3Key"`
	Genres    []*MusicGenre `json:"genres"`
	Artists   []*Artist     `json:"artists"`
	Songs     []*Song       `json:"songs"`
	Year      *int          `json:"year"`
	Label     *string       `json:"label"`
	CoverURL  *string       `json:"coverUrl"`
}

type AlbumResults struct {
	Total  *int     `json:"total"`
	Albums []*Album `json:"albums"`
}

type Artist struct {
	ID        string        `json:"id"`
	CreatedAt time.Time     `json:"createdAt"`
	UpdatedAt time.Time     `json:"updatedAt"`
	Name      string        `json:"name"`
	CoverURL  *string       `json:"coverUrl"`
	Genres    []*MusicGenre `json:"genres"`
	Songs     []*Song       `json:"songs"`
	Albums    []*Album      `json:"albums"`
}

type ArtistResults struct {
	Total   *int      `json:"total"`
	Artists []*Artist `json:"artists"`
}

type Auth struct {
	User         *User  `json:"user"`
	AccessToken  string `json:"accessToken"`
	RefreshToken string `json:"refreshToken"`
}

type Device struct {
	ID             string    `json:"id"`
	CreatedAt      time.Time `json:"createdAt"`
	UpdatedAt      time.Time `json:"updatedAt"`
	LastConnection time.Time `json:"lastConnection"`
	UserAgent      string    `json:"userAgent"`
	Type           string    `json:"type"`
	Os             string    `json:"os"`
	Name           string    `json:"name"`
	LastIP         string    `json:"lastIP"`
	LastLocation   *string   `json:"lastLocation"`
}

type GenreResults struct {
	Total  *int          `json:"total"`
	Genres []*MusicGenre `json:"genres"`
}

type ItemsInput struct {
	Ids            []string `json:"ids"`
	CollectionName *string  `json:"collectionName"`
}

type MusicGenre struct {
	ID     int      `json:"id"`
	Name   string   `json:"name"`
	Type   string   `json:"type"`
	Songs  []*Song  `json:"songs"`
	Albums []*Album `json:"albums"`
}

type NewUser struct {
	Name     string `json:"name"`
	Password string `json:"password"`
}

type PaginationInput struct {
	Count *int `json:"count"`
	Page  *int `json:"page"`
}

type Playlist struct {
	ID          string        `json:"id"`
	CreatedAt   time.Time     `json:"createdAt"`
	UpdatedAt   time.Time     `json:"updatedAt"`
	Creator     *User         `json:"creator"`
	Songs       []*Song       `json:"songs"`
	Genres      []*MusicGenre `json:"genres"`
	Name        string        `json:"name"`
	CoverURL    *string       `json:"coverUrl"`
	Description *string       `json:"description"`
}

type PlaylistResults struct {
	Total     *int        `json:"total"`
	Playlists []*Playlist `json:"playlists"`
}

type S3Content struct {
	Songs   int `json:"songs"`
	Albums  int `json:"albums"`
	Artists int `json:"artists"`
	Updated int `json:"updated"`
}

type S3Credential struct {
	ID           string    `json:"id"`
	CreatedAt    time.Time `json:"createdAt"`
	UpdatedAt    time.Time `json:"updatedAt"`
	KeyID        *string   `json:"keyID"`
	KeySecret    *string   `json:"keySecret"`
	BucketHost   string    `json:"bucketHost"`
	BucketName   string    `json:"bucketName"`
	ClientRegion *string   `json:"clientRegion"`
	BucketRegion *string   `json:"bucketRegion"`
}

type S3CredentialCreateInput struct {
	KeyID        string  `json:"keyId"`
	KeySecret    string  `json:"keySecret"`
	BucketHost   string  `json:"bucketHost"`
	BucketName   string  `json:"bucketName"`
	ClientRegion *string `json:"clientRegion"`
	BucketRegion *string `json:"bucketRegion"`
}

type S3CredentialUpdateInput struct {
	KeyID        *string `json:"keyId"`
	KeySecret    *string `json:"keySecret"`
	BucketHost   *string `json:"bucketHost"`
	BucketName   *string `json:"bucketName"`
	ClientRegion *string `json:"clientRegion"`
	BucketRegion *string `json:"bucketRegion"`
}

type S3SignedURL struct {
	ID        string    `json:"id"`
	SignedURL string    `json:"signedUrl"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
	ExpireAt  time.Time `json:"expireAt"`
}

type SearchResults struct {
	Total    *int             `json:"total"`
	Artists  *ArtistResults   `json:"artists"`
	Albums   *AlbumResults    `json:"albums"`
	Songs    *SongResults     `json:"songs"`
	Playlist *PlaylistResults `json:"playlist"`
	Genres   *GenreResults    `json:"genres"`
}

type SearchSongInput struct {
	ArtistName *string `json:"artistName"`
	AlbumName  *string `json:"albumName"`
	SongName   *string `json:"songName"`
	Random     *bool   `json:"random"`
}

type SharingRequest struct {
	ID        string    `json:"id"`
	To        *User     `json:"to"`
	From      *User     `json:"from"`
	Refused   *bool     `json:"refused"`
	Accepted  bool      `json:"accepted"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"updatedAt"`
}

type Song struct {
	ID            string        `json:"id"`
	CreatedAt     time.Time     `json:"createdAt"`
	UpdatedAt     time.Time     `json:"updatedAt"`
	DownloadCount int           `json:"downloadCount"`
	Artist        *Artist       `json:"artist"`
	Album         *Album        `json:"album"`
	Title         string        `json:"title"`
	Duration      string        `json:"duration"`
	CoverURL      *string       `json:"coverUrl"`
	S3Key         string        `json:"s3Key"`
	SignedURL     *S3SignedURL  `json:"signedUrl"`
	Genres        []*MusicGenre `json:"genres"`
}

type SongResults struct {
	Total *int    `json:"total"`
	Songs []*Song `json:"songs"`
}

type User struct {
	ID                      string            `json:"id"`
	CreatedAt               time.Time         `json:"createdAt"`
	UpdatedAt               time.Time         `json:"updatedAt"`
	Name                    string            `json:"name"`
	S3Credential            *S3Credential     `json:"s3Credential"`
	S3SharedCredential      []*S3Credential   `json:"s3SharedCredential"`
	Devices                 []*Device         `json:"devices"`
	Songs                   []*Song           `json:"songs"`
	Playlists               []*Playlist       `json:"playlists"`
	Albums                  []*Album          `json:"albums"`
	ReceivedSharingRequests []*SharingRequest `json:"receivedSharingRequests"`
	AskedSharingRequests    []*SharingRequest `json:"askedSharingRequests"`
}

type UserItem struct {
	ID             string    `json:"id"`
	CreatedAt      time.Time `json:"createdAt"`
	UpdatedAt      time.Time `json:"updatedAt"`
	Name           string    `json:"name"`
	SongsCount     *int      `json:"songsCount"`
	AlbumsCount    *int      `json:"albumsCount"`
	PlaylistsCount *int      `json:"playlistsCount"`
}

type UserResults struct {
	Total *int        `json:"total"`
	Users []*UserItem `json:"users"`
}
