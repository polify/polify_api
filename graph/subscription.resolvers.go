package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/bogem/id3v2"
	"github.com/cespare/xxhash"
	cloudinary "github.com/cloudinary/cloudinary-go"
	"github.com/cloudinary/cloudinary-go/api"
	"github.com/cloudinary/cloudinary-go/api/admin"
	"github.com/cloudinary/cloudinary-go/api/uploader"
	minio "github.com/minio/minio-go/v7"
	"gitlab.com/polify/polify_api/bucket"
	"gitlab.com/polify/polify_api/db"
	"gitlab.com/polify/polify_api/graph/generated"
	"gitlab.com/polify/polify_api/graph/model"
	"gitlab.com/polify/polify_api/middlewares"
)

func (r *subscriptionResolver) Updatesongsmetas(ctx context.Context, withduration *bool) (<-chan *model.Song, error) {
	var (
		err              error
		client           *bucket.Bucket
		cld              *cloudinary.Cloudinary
		usrCtx           *middlewares.UserContext
		songs            []db.SongModel
		allreadyUploaded *admin.AssetsResult
	)
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		return nil, errors.New("missing auth")
	}
	if client, err = bucket.GetUserBucketClient(r.DB, usrCtx.ID); err != nil {
		return nil, errors.New("internal system error")
	}
	if cld, err = cloudinary.NewFromParams(
		os.Getenv("CLOUDINARY_BUCKET"),
		os.Getenv("CLOUDINARY_ID"),
		os.Getenv("CLOUDINARY_SECRET"),
	); err != nil {
		log.Errorf(" Failed to load cloudinary client : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}
	if allreadyUploaded, err = cld.Admin.Assets(ctx, admin.AssetsParams{
		// Prefix:     "covers",
		// Prefix:     "samples/*",
		AssetType:  api.Image,
		MaxResults: 400,
	}); err != nil {
		log.Errorf(" Failed to load existing files : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}

	fmt.Printf("[INFO] Got %v existing asset on cloudinary\n", len(allreadyUploaded.Assets))
	if songs, err = r.DB.Song.FindMany(
		db.Song.OwnerID.Equals(usrCtx.ID),
		db.Song.CoverURL.IsNull(),
		db.Song.Or(db.Song.Duration.Equals("")),
	).
		With(
			db.Song.Album.Fetch(),
			db.Song.Artist.Fetch(),
			db.Song.Genres.Fetch(),
		).
		OrderBy(
			db.Song.AlbumID.Order(db.ASC),
		).
		Exec(ctx); err != nil {
		return nil, errors.New("internal system error")
	}

	var res = make(chan *model.Song, len(songs))

	go func() {
		for cnt, song := range songs {
			var (
				songDuration        *string
				previousSongAlbumID *string
				currentSongAlbumID  *string
				albumCoverURL       *string
				currentAlbum        *db.AlbumModel
				picture             []byte
				picFname            string
			)
			if v, ok := song.AlbumID(); ok {
				currentSongAlbumID = &v
			}
			if cnt-1 > 0 {
				if v, ok := songs[cnt-1].AlbumID(); ok {
					previousSongAlbumID = &v
				}
			}
			if previousSongAlbumID != nil && currentSongAlbumID != nil &&
				*previousSongAlbumID == *currentSongAlbumID {
				currentAlbum, err = r.DB.Album.FindUnique(
					db.Album.ID.EqualsIfPresent(currentSongAlbumID),
				).Exec(ctx)
				if err != nil {
					log.Errorf(" Failed to get album : %s\n", err.Error())
					continue
				}
			} else if currentSongAlbumID != nil {
				currentAlbum, err = r.DB.Album.FindUnique(
					db.Album.ID.EqualsIfPresent(currentSongAlbumID),
				).Exec(ctx)
				if err != nil {
					log.Errorf(" Failed to get album : %s\n", err.Error())
					continue
				}
			}
			exist := fmt.Sprintf("%v-%v", xxhash.Sum64String(song.S3Key), 1)
			for _, el := range allreadyUploaded.Assets {
				chck := strings.ReplaceAll(el.PublicID, "/"+os.Getenv("CLOUDINARY_DIRECTORY"), "")
				// fmt.Printf("Will check '%s' against '%s'\n", exist, chck)
				if exist == chck {
					fmt.Printf("[EL - %s] %s | exist : %v\n", exist, chck, exist == chck)
					if currentAlbum != nil {
						if v, ok := currentAlbum.CoverURL(); ok {
							albumCoverURL = &v
						} else {
							inf, _ := cld.Admin.Asset(ctx, admin.AssetParams{
								PublicID: el.PublicID,
							})

							if al, ok := song.Album(); ok && al != nil {
								fmt.Printf("[INFO] Will update album %s\n", al.ID)
								if _, err = r.DB.Album.FindUnique(
									db.Album.ID.Equals(al.ID),
								).Update(
									db.Album.CoverURL.Set(inf.SecureURL),
								).
									Exec(ctx); err != nil {
									log.Errorf(" failed to update album : %s\n",
										err.Error())
								}
							} else {
								log.Errorf(" Song %s has no album\n", song.ID)
							}
						}

					}

					var up *db.SongModel
					fmt.Printf("[INFO] Will update song %s\n", song.ID)
					if up, err = r.DB.Song.FindUnique(
						db.Song.ID.Equals(song.ID),
					).Update(
						db.Song.CoverURL.SetIfPresent(albumCoverURL),
					).
						Exec(ctx); err != nil {
						log.Errorf(" failed to update song : %s\n",
							err.Error())
						continue
					}
					cvrt := model.Song{
						ID:            up.ID,
						CreatedAt:     up.CreatedAt,
						UpdatedAt:     up.UpdatedAt,
						DownloadCount: int(up.DownloadCount),
						Title:         up.Title,
						CoverURL:      albumCoverURL,
					}
					res <- &cvrt
					continue
				}
			}
			// os.Exit(0)
			obj, err := client.Client.GetObject(ctx, client.Name, song.S3Key, minio.GetObjectOptions{})
			if err != nil {
				log.Errorf(" Failed to get object '%s' : %s\n", song.S3Key, err.Error())
				continue
			}
			st, _ := obj.Stat()
			fmt.Printf("[INFO] Getting obj %+v\n", st)
			defer obj.Close()
			tag, err := id3v2.ParseReader(obj, id3v2.Options{Parse: true})
			if err != nil {
				log.Errorf(" Failed to read tags : %s\n", err.Error())
				// return nil, errors.New("internal system error")
				continue
			}
			defer tag.Close()
			if withduration != nil && *withduration {
				fmt.Printf("HEllo\n")
				// d, err := mp3.NewDecoder(obj)
				// if err != nil {
				// 	log.Errorf(" Failed to open mp3 : %s\n", err.Error())
				// 	// return nil, errors.New("internal system error")
				// 	continue
				// }

				// const sampleSize = int64(4)                    // From documentation.
				// samples := d.Length() / sampleSize             // Number of samples.
				// audioLength := samples / int64(d.SampleRate()) // Audio length in seconds.
				// fmt.Printf("[INFO] Title duration is %v seconds\n", audioLength)
				// val := time.Duration(audioLength).String()
				// songDuration = &val
				// if _, err = r.DB.Song.FindUnique(
				// 	db.Song.ID.Equals(song.ID),
				// ).Update(
				// 	db.Song.Duration.SetIfPresent(songDuration),
				// ).
				// 	Exec(ctx); err != nil {
				// 	log.Errorf(" failed to update song : %s\n",
				// 		err.Error())
				// 	continue
				// }
			}
			if currentAlbum != nil {
				if v, ok := currentAlbum.CoverURL(); ok {
					albumCoverURL = &v
				} else {
					pictures := tag.GetFrames("APIC")
					if len(pictures) == 0 {
						fmt.Printf("[INFO] No image tag\n")
						continue
					}
					pic, ok := pictures[0].(id3v2.PictureFrame)
					if !ok {
						fmt.Printf("[INFO] Couldn't assert picture frame\n")
						continue
					}
					picFname = fmt.Sprintf("%v-%v", xxhash.Sum64String(song.S3Key), 1)
					picture = pic.Picture
					upl := cld.Upload
					uploadRes, err := upl.Upload(ctx, bytes.NewReader(picture), uploader.UploadParams{
						PublicID: picFname,
						Folder:   os.Getenv("CLOUDINARY_DIRECTORY"),
					})
					if err != nil {
						log.Errorf(" Failed to upload cover image : %s\n",
							err.Error())
						continue
					}
					albumCoverURL = &uploadRes.SecureURL
					if al, ok := song.Album(); ok && al != nil {
						fmt.Printf("[INFO] Will update album %s\n", al.ID)
						if _, err = r.DB.Album.FindUnique(
							db.Album.ID.Equals(al.ID),
						).Update(
							db.Album.Name.Set(tag.Album()),
							db.Album.CoverURL.Set(uploadRes.SecureURL),
						).
							Exec(ctx); err != nil {
							log.Errorf(" failed to update album : %s\n",
								err.Error())
						}
					} else {
						log.Errorf(" Song %s has no album\n", song.ID)
					}
				}

			}

			var up *db.SongModel
			fmt.Printf("[INFO] Will update song %s\n", song.ID)
			if up, err = r.DB.Song.FindUnique(
				db.Song.ID.Equals(song.ID),
			).Update(
				db.Song.Title.Set(tag.Title()),
				db.Song.CoverURL.SetIfPresent(albumCoverURL),
				db.Song.Duration.SetIfPresent(songDuration),
			).
				Exec(ctx); err != nil {
				log.Errorf(" failed to update song : %s\n",
					err.Error())
				continue
			}
			cvrt := model.Song{
				ID:            up.ID,
				CreatedAt:     up.CreatedAt,
				UpdatedAt:     up.UpdatedAt,
				DownloadCount: int(up.DownloadCount),
				Title:         up.Title,
				CoverURL:      albumCoverURL,
			}
			res <- &cvrt
		}
	}()
	return res, nil
}

func (r *subscriptionResolver) Syncs3(ctx context.Context) (<-chan *int, error) {
	panic(fmt.Errorf("not implemented"))
}

// Subscription returns generated.SubscriptionResolver implementation.
func (r *Resolver) Subscription() generated.SubscriptionResolver { return &subscriptionResolver{r} }

type subscriptionResolver struct{ *Resolver }
