package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"errors"
	"fmt"
	"os"
	"time"

	sillyname "github.com/Pallinder/sillyname-go"
	"github.com/prisma/prisma-client-go/runtime/types"
	"gitlab.com/polify/polify_api/db"
	"gitlab.com/polify/polify_api/graph/generated"
	"gitlab.com/polify/polify_api/graph/model"
	"gitlab.com/polify/polify_api/jwt"

	"gitlab.com/polify/polify_api/middlewares"
	"golang.org/x/crypto/bcrypt"
)

func (r *queryResolver) Search(ctx context.Context, query string) (*model.SearchResults, error) {
	var err error
	var usrCtx *middlewares.UserContext
	var res = model.SearchResults{}
	var artistsResults = []map[string]string{}
	var albumsResults = []map[string]string{}
	var songsResults = []map[string]string{}
	var playlistsResults = []map[string]string{}
	var genresResults = []map[string]interface{}{}
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'search' query : %s\n", err.Error())
		return nil, err
	}
	shares, err := GetSharedAccessUserIDs(usrCtx.ID, r.DB)
	if err != nil {
		log.Errorf("[ERROR] Failed to get shared with : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}

	if err = r.DB.Prisma.QueryRaw(`
	SELECT id, name, "createdAt", "updatedAt", "coverUrl" FROM polify."Artist" WHERE to_tsvector('french',name) @@ plainto_tsquery('french',$1);
	`, query).Exec(ctx, &artistsResults); err != nil {
		log.Errorf("[ERROR] Failed to search artist : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	// TODO : Find a proper way to do full text search without being "stopped" by "stop word" cf plainto_tsquery("english", "i am")
	if err = r.DB.Prisma.QueryRaw(`
	SELECT id, name, "createdAt", "updatedAt", "coverUrl" FROM polify."Album" WHERE to_tsvector('french',name) @@ plainto_tsquery('french',$1) AND "ownerId" = ANY($2); 
	`, query, shares).Exec(ctx, &albumsResults); err != nil {
		log.Printf("[ERROR] Failed to search albums : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	if err = r.DB.Prisma.QueryRaw(`
	SELECT id, title, duration, "createdAt", "updatedAt", "coverUrl" FROM polify."Song" WHERE to_tsvector('french',title) @@ plainto_tsquery('french',$1) AND "ownerId" = ANY($2); 
	`, query, shares).Exec(ctx, &songsResults); err != nil {
		log.Printf("[ERROR] Failed to search songs : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	if err = r.DB.Prisma.QueryRaw(`
	SELECT id, name, "createdAt", "updatedAt" from polify."Playlist" WHERE to_tsvector('french',name) @@ plainto_tsquery('french',$1) AND "creatorId" = ANY($2); 
	`, query, shares).Exec(ctx, &playlistsResults); err != nil {
		log.Printf("[ERROR] Failed to search playlist : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	if err = r.DB.Prisma.QueryRaw(`
	SELECT id, name, type from polify."MusicGenre" WHERE to_tsvector('french',name) @@ plainto_tsquery('french',$1); 
	`, query).Exec(ctx, &genresResults); err != nil {
		log.Printf("[ERROR] Failed to search genres : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	totAlbums := len(albumsResults)
	totArtists := len(artistsResults)
	totSongs := len(songsResults)
	totPlaylist := len(playlistsResults)
	totGenres := len(genresResults)
	totTot := totAlbums + totArtists + totSongs + totPlaylist + totGenres
	res.Total = &totTot
	if totAlbums > 0 {
		res.Albums = &model.AlbumResults{
			Total:  &totAlbums,
			Albums: []*model.Album{},
		}
		for el := range albumsResults {
			var coverUrl *string
			if v, ok := albumsResults[el]["coverUrl"]; ok && v != "" {
				coverUrl = &v
			}
			createdAt, err := time.Parse(PGTIMESTAMPFORMAT, albumsResults[el]["createdAt"])
			if err != nil {
				log.Panicf(err.Error())
			}
			updatedAt, err := time.Parse(PGTIMESTAMPFORMAT, albumsResults[el]["updatedAt"])
			if err != nil {
				log.Panicf(err.Error())
			}

			res.Albums.Albums = append(res.Albums.Albums, &model.Album{
				ID:        albumsResults[el]["id"],
				Name:      albumsResults[el]["name"],
				CreatedAt: createdAt,
				UpdatedAt: updatedAt,
				CoverURL:  coverUrl,
			})
		}
	}
	if totArtists > 0 {
		res.Artists = &model.ArtistResults{
			Total:   &totArtists,
			Artists: []*model.Artist{},
		}
		for el := range artistsResults {
			var coverUrl *string
			if v, ok := artistsResults[el]["coverUrl"]; ok && v != "" {
				coverUrl = &v
			}
			createdAt, err := time.Parse(PGTIMESTAMPFORMAT, artistsResults[el]["createdAt"])
			if err != nil {
				log.Panicf(err.Error())
			}
			updatedAt, err := time.Parse(PGTIMESTAMPFORMAT, artistsResults[el]["updatedAt"])
			if err != nil {
				log.Panicf(err.Error())
			}
			res.Artists.Artists = append(res.Artists.Artists, &model.Artist{
				ID:        artistsResults[el]["id"],
				Name:      artistsResults[el]["name"],
				CoverURL:  coverUrl,
				CreatedAt: createdAt,
				UpdatedAt: updatedAt,
			})
		}
	}
	if totSongs > 0 {
		res.Songs = &model.SongResults{
			Total: &totSongs,
			Songs: []*model.Song{},
		}
		for el := range songsResults {
			var coverUrl *string
			if v, ok := songsResults[el]["coverUrl"]; ok && v != "" {
				coverUrl = &v
			}
			createdAt, err := time.Parse(PGTIMESTAMPFORMAT, songsResults[el]["createdAt"])
			if err != nil {
				log.Panicf(err.Error())
			}
			updatedAt, err := time.Parse(PGTIMESTAMPFORMAT, songsResults[el]["updatedAt"])
			if err != nil {
				log.Panicf(err.Error())
			}
			res.Songs.Songs = append(res.Songs.Songs, &model.Song{
				ID:        songsResults[el]["id"],
				Title:     songsResults[el]["title"],
				Duration:  songsResults[el]["duration"],
				CoverURL:  coverUrl,
				CreatedAt: createdAt,
				UpdatedAt: updatedAt,
			})
		}
	}
	if totPlaylist > 0 {
		res.Playlist = &model.PlaylistResults{
			Total:     &totPlaylist,
			Playlists: []*model.Playlist{},
		}
		for el := range playlistsResults {
			createdAt, err := time.Parse(PGTIMESTAMPFORMAT, playlistsResults[el]["createdAt"])
			if err != nil {
				log.Panicf(err.Error())
			}
			updatedAt, err := time.Parse(PGTIMESTAMPFORMAT, playlistsResults[el]["updatedAt"])
			if err != nil {
				log.Panicf(err.Error())
			}
			res.Playlist.Playlists = append(res.Playlist.Playlists, &model.Playlist{
				ID:        playlistsResults[el]["id"],
				Name:      playlistsResults[el]["name"],
				CreatedAt: createdAt,
				UpdatedAt: updatedAt,
			})
		}
	}
	if totGenres > 0 {
		res.Genres = &model.GenreResults{
			Total:  &totGenres,
			Genres: []*model.MusicGenre{},
		}
		for el := range genresResults {
			nMGenre := &model.MusicGenre{}
			if v, ok := genresResults[el]["id"].(float64); ok {
				nMGenre.ID = int(v)
			}
			if v, ok := genresResults[el]["name"].(string); ok {
				nMGenre.Name = v
			}
			if v, ok := genresResults[el]["type"].(string); ok {
				nMGenre.Type = v
			}
			res.Genres.Genres = append(res.Genres.Genres, nMGenre)
		}
	}
	return &res, nil
}

func (r *queryResolver) Songs(ctx context.Context, pagination *model.PaginationInput) (*model.SongResults, error) {
	var err error
	var songs []db.SongModel
	var rt []*model.Song
	var res *model.SongResults
	var usrCtx *middlewares.UserContext
	var defCount = 100
	var defOffset = 0
	var results = []map[string]int{}
	var defPagination = model.PaginationInput{
		Count: &defCount,
		Page:  &defOffset,
	}
	if pagination != nil {
		if pagination.Count != nil {
			defPagination.Count = pagination.Count
		}
		if pagination.Page != nil {
			defPagination.Page = pagination.Page
		}
	}

	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'songs' query : %s\n", err.Error())
		return nil, err
	}
	shares, err := GetSharedAccessUserIDs(usrCtx.ID, r.DB)
	if err != nil {
		log.Errorf("[ERROR] Failed to get shared with : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	if err = r.DB.Prisma.QueryRaw(
		`SELECT COUNT(id) FROM polify."Song" WHERE "ownerId" = ANY($1);`,
		shares,
	).Exec(ctx, &results); err != nil {
		log.Errorf("Failed to count song : %s\n", err.Error())
		return nil, errors.New("server error")
	}
	if songs, err = r.DB.Song.FindMany(
		db.Song.OwnerID.In(shares),
	).
		With(
			db.Song.Album.Fetch(),
			db.Song.Artist.Fetch(),
			db.Song.Genres.Fetch(),
		).
		Skip(*defPagination.Count * *defPagination.Page).
		Take(*defPagination.Count).
		Exec(ctx); err != nil {
		log.Errorf("Failed to list songs : %s\n", err.Error())

		return nil, errors.New("internal system error")
	}
	if rt, err = GetSongsModel(r.DB, songs, false); err != nil {
		return nil, errors.New("internal system error")
	}
	val := results[0]["count"]
	res = &model.SongResults{
		Total: &val,
		Songs: rt,
	}
	return res, nil
}

func (r *queryResolver) Song(ctx context.Context, id string) (*model.Song, error) {
	var err error
	var song *db.SongModel
	var rt *model.Song
	var usrCtx *middlewares.UserContext

	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'song' query : %s\n", err.Error())
		return nil, err
	}
	// shares, err := GetSharedAccessUserIDs(usrCtx.ID, r.DB)
	// if err != nil {
	// 	log.Errorf("[ERROR] Failed to get shared with : %s\n", err.Error())
	// 	return nil, errors.New("internal server error")
	// }
	if song, err = r.DB.Song.FindUnique(
		db.Song.ID.Equals(id),
		// db.Song.And(
		// 	db.Song.OwnerID.In(shares),
		// ),
	).With(
		db.Song.Album.Fetch(),
		db.Song.Artist.Fetch(),
		db.Song.Genres.Fetch(),
		db.Song.S3SignedURL.Fetch(),
	).Exec(ctx); err != nil {
		log.Errorf("Failed to find song : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	var nElSong = []*model.Song{}
	if nElSong, err = GetSongsModel(r.DB, []db.SongModel{*song}, true); err != nil {
		return nil, errors.New("internal system error")
	}
	rt = nElSong[0]
	if _, err = r.DB.Song.FindUnique(db.Song.ID.Equals(rt.ID)).Update(
		db.Song.DownloadCount.Set(types.BigInt(rt.DownloadCount + 1)),
	).Exec(ctx); err != nil {
		log.Errorf("failed to update downloaded count : %s\n", err.Error())
	}
	return rt, nil
}

func (r *queryResolver) SearchSongs(ctx context.Context, pagination *model.PaginationInput, input model.SearchSongInput) ([]*model.Song, error) {
	var err error
	var songs []db.SongModel
	var rt []*model.Song
	var usrCtx *middlewares.UserContext
	var defCount = 100
	var defOffset = 0
	var defOrder = db.ASC
	var defPagination = model.PaginationInput{
		Count: &defCount,
		Page:  &defOffset,
	}
	if pagination != nil {
		if pagination.Count != nil {
			defPagination.Count = pagination.Count
		}
		if pagination.Page != nil {
			defPagination.Page = pagination.Page
		}
	}
	if input.Random != nil && *input.Random {
		defOrder = "random()"
	}
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'searchsong' query : %s\n", err.Error())
		return nil, err
	}
	shares, err := GetSharedAccessUserIDs(usrCtx.ID, r.DB)
	if err != nil {
		log.Errorf("[ERROR] Failed to get shared with : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	if songs, err = r.DB.Song.FindMany(
		db.Song.OwnerID.In(shares),
		db.Song.Title.ContainsIfPresent(input.SongName),
		db.Song.Artist.Where(db.Artist.Name.ContainsIfPresent(input.ArtistName)),
		db.Song.Album.Where(db.Album.Name.ContainsIfPresent(input.AlbumName)),
	).
		With(
			db.Song.Album.Fetch(),
			db.Song.Artist.Fetch(),
			db.Song.Genres.Fetch(),
		).
		Skip(*defPagination.Count * *defPagination.Page).
		Take(*defPagination.Count).
		OrderBy(
			db.Song.ID.Order(defOrder),
		).
		Exec(ctx); err != nil {
		log.Errorf("failed to search song : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	if rt, err = GetSongsModel(r.DB, songs, false); err != nil {
		return nil, errors.New("internal system error")
	}
	return rt, nil
}

func (r *queryResolver) Artists(ctx context.Context, pagination *model.PaginationInput) ([]*model.Artist, error) {
	var err error
	var artists []db.ArtistModel
	var rt []*model.Artist
	var usrCtx *middlewares.UserContext
	var defCount = 100
	var defOffset = 0
	var defPagination = model.PaginationInput{
		Count: &defCount,
		Page:  &defOffset,
	}
	if pagination != nil {
		if pagination.Count != nil {
			defPagination.Count = pagination.Count
		}
		if pagination.Page != nil {
			defPagination.Page = pagination.Page
		}
	}
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'artists' query : %s\n", err.Error())
		return nil, err
	}
	shares, err := GetSharedAccessUserIDs(usrCtx.ID, r.DB)
	if err != nil {
		log.Errorf("[ERROR] Failed to get shared with : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	if artists, err = r.DB.Artist.FindMany().
		With(
			db.Artist.Albums.Fetch(),
			db.Artist.Songs.Fetch(
				db.Song.OwnerID.In(shares),
			),
			db.Artist.Genres.Fetch(),
		).
		Skip(*defPagination.Count * *defPagination.Page).
		Take(*defPagination.Count).
		Exec(ctx); err != nil {
		log.Errorf("failed to search artists : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	for el := range artists {
		var artist = artists[el]
		nEl := &model.Artist{
			ID:        artist.ID,
			CreatedAt: artist.CreatedAt,
			UpdatedAt: artist.UpdatedAt,
			Name:      artist.Name,
		}
		if v, ok := artist.CoverURL(); ok {
			nEl.CoverURL = &v
		}
		if v := artist.Albums(); v != nil {
			for el := range v {
				nAlbum := &model.Album{
					ID:        v[el].ID,
					CreatedAt: v[el].CreatedAt,
					UpdatedAt: v[el].UpdatedAt,
					Name:      v[el].Name,
					S3Key:     v[el].S3Key,
				}
				if v, ok := v[el].Year(); ok {
					nAlbum.Year = &v
				}
				if v, ok := v[el].CoverURL(); ok {
					nAlbum.CoverURL = &v
				}
				nEl.Albums = append(nEl.Albums, nAlbum)
			}
		}
		if v := artist.Songs(); v != nil {
			for el := range v {
				nEl.Songs = append(nEl.Songs, &model.Song{
					ID:        v[el].ID,
					CreatedAt: v[el].CreatedAt,
					UpdatedAt: v[el].UpdatedAt,
					Title:     v[el].Title,
					S3Key:     v[el].S3Key,
					// Year: v[el].Year,
				})
			}

		}
		genres := artist.Genres()
		for ind := range genres {
			nEl.Genres = append(nEl.Genres, &model.MusicGenre{
				ID:   genres[ind].ID,
				Name: genres[ind].Name,
			})
		}
		rt = append(rt, nEl)
	}
	return rt, nil
}

func (r *queryResolver) Artist(ctx context.Context, id string) (*model.Artist, error) {
	var err error
	var artist *db.ArtistModel
	var rt *model.Artist
	var usrCtx *middlewares.UserContext

	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'artist' query : %s\n", err.Error())
		return nil, err
	}
	shares, err := GetSharedAccessUserIDs(usrCtx.ID, r.DB)
	if err != nil {
		log.Errorf("[ERROR] Failed to get shared with : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	if artist, err = r.DB.Artist.FindUnique(
		// db.Artist.OwnerID.Equals(usrCtx.ID),
		db.Artist.ID.Equals(id),
	).With(
		db.Artist.Albums.Fetch(
			db.Album.OwnerID.In(shares),
		),
		db.Artist.Songs.Fetch(
			db.Song.OwnerID.In(shares),
		),
		db.Artist.Genres.Fetch(),
	).
		Exec(ctx); err != nil {
		log.Errorf("Error getting artist : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	nEl := &model.Artist{
		ID:        artist.ID,
		CreatedAt: artist.CreatedAt,
		UpdatedAt: artist.UpdatedAt,
		Name:      artist.Name,
	}
	if v, ok := artist.CoverURL(); ok {
		nEl.CoverURL = &v
	}
	if v := artist.Albums(); v != nil {
		for el := range v {
			nAlbum := &model.Album{
				ID:        v[el].ID,
				CreatedAt: v[el].CreatedAt,
				UpdatedAt: v[el].UpdatedAt,
				Name:      v[el].Name,
				S3Key:     v[el].S3Key,
				// Year: v[el].Year,
			}
			if v, ok := v[el].CoverURL(); ok {
				nAlbum.CoverURL = &v
			}
			if v, ok := v[el].Year(); ok {
				nAlbum.Year = &v
			}
			nEl.Albums = append(nEl.Albums, nAlbum)
		}
	}
	if v := artist.Songs(); v != nil {
		for el := range v {
			nEl.Songs = append(nEl.Songs, &model.Song{
				ID:        v[el].ID,
				CreatedAt: v[el].CreatedAt,
				UpdatedAt: v[el].UpdatedAt,
				Title:     v[el].Title,
				S3Key:     v[el].S3Key,
				// Year: v[el].Year,
			})
		}
	}
	genres := artist.Genres()
	for ind := range genres {
		rt.Genres = append(rt.Genres, &model.MusicGenre{
			ID:   genres[ind].ID,
			Name: genres[ind].Name,
		})
	}
	rt = nEl
	return rt, nil
}

func (r *queryResolver) Albums(ctx context.Context, pagination *model.PaginationInput) (*model.AlbumResults, error) {
	var err error
	var albums []db.AlbumModel
	var rt []*model.Album
	var usrCtx *middlewares.UserContext
	var results = []map[string]int{}
	var defCount = 100
	var defOffset = 0
	var defPagination = model.PaginationInput{
		Count: &defCount,
		Page:  &defOffset,
	}
	if pagination != nil {
		if pagination.Count != nil {
			defPagination.Count = pagination.Count
		}
		if pagination.Page != nil {
			defPagination.Page = pagination.Page
		}
	}

	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'albums' query : %s\n", err.Error())
		return nil, err
	}
	shares, err := GetSharedAccessUserIDs(usrCtx.ID, r.DB)
	if err != nil {
		log.Errorf("[ERROR] Failed to get shared with : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	if err = r.DB.Prisma.QueryRaw(
		`SELECT COUNT(id) FROM polify."Album" WHERE "ownerId" = ANY($1);`,
		shares,
	).Exec(ctx, &results); err != nil {
		log.Errorf("Error counting albums : %s\n", err.Error())
		return nil, errors.New("server error")
	}
	if albums, err = r.DB.Album.FindMany(
		db.Album.OwnerID.In(shares),
	).
		With(
			db.Album.Artists.Fetch(),
			db.Album.Songs.Fetch(),
			db.Album.Genres.Fetch(),
		).
		Skip(*defPagination.Count * *defPagination.Page).
		Take(*defPagination.Count).
		Exec(ctx); err != nil {
		log.Errorf("Error searching albums : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}
	for el := range albums {
		nEl := &model.Album{
			ID:        albums[el].ID,
			Name:      albums[el].Name,
			CreatedAt: albums[el].CreatedAt,
			UpdatedAt: albums[el].UpdatedAt,
			S3Key:     albums[el].S3Key,
		}
		if v, ok := albums[el].Year(); ok {
			nEl.Year = &v
		}
		if v, ok := albums[el].CoverURL(); ok {
			nEl.CoverURL = &v
		}
		if v, ok := albums[el].Label(); ok {
			nEl.Label = &v
		}
		if v := albums[el].Artists(); len(v) > 0 {
			for el := range v {
				nEl.Artists = append(nEl.Artists, &model.Artist{
					ID:        v[el].ID,
					Name:      v[el].Name,
					CreatedAt: v[el].CreatedAt,
					UpdatedAt: v[el].UpdatedAt,
				})
			}

		}
		if v := albums[el].Songs(); len(v) > 0 {
			for el := range v {
				nSong := &model.Song{
					ID:        v[el].ID,
					Title:     v[el].Title,
					CreatedAt: v[el].CreatedAt,
					UpdatedAt: v[el].UpdatedAt,
					S3Key:     v[el].S3Key,
				}
				if v, ok := v[el].CoverURL(); ok {
					nSong.CoverURL = &v
				}
				nEl.Songs = append(nEl.Songs, nSong)
			}

		}
		genres := albums[el].Genres()
		for ind := range genres {
			nEl.Genres = append(nEl.Genres, &model.MusicGenre{
				ID:   genres[ind].ID,
				Name: genres[ind].Name,
			})
		}
		rt = append(rt, nEl)
	}
	val := results[0]["count"]
	res := model.AlbumResults{
		Total:  &val,
		Albums: rt,
	}
	return &res, nil
}

func (r *queryResolver) Album(ctx context.Context, id string) (*model.Album, error) {
	var err error
	var album *db.AlbumModel
	var rt *model.Album
	var usrCtx *middlewares.UserContext

	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'album' query : %s\n", err.Error())
		return nil, err
	}
	if album, err = r.DB.Album.FindUnique(
		db.Album.ID.Equals(id),
	).With(
		db.Album.Artists.Fetch(),
		db.Album.Songs.Fetch().With(
			db.Song.Artist.Fetch(),
			db.Song.Album.Fetch(),
			db.Song.Genres.Fetch(),
			db.Song.S3SignedURL.Fetch(),
		),
		db.Album.Genres.Fetch(),
	).
		Exec(ctx); err != nil {
		log.Errorf("Error getting album : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	nEl := &model.Album{
		ID:        album.ID,
		Name:      album.Name,
		CreatedAt: album.CreatedAt,
		UpdatedAt: album.UpdatedAt,
		S3Key:     album.S3Key,
	}
	if v, ok := album.CoverURL(); ok {
		nEl.CoverURL = &v
	}
	if v, ok := album.Label(); ok {
		nEl.Label = &v
	}
	if v := album.Artists(); len(v) > 0 {
		for el := range v {
			nEl.Artists = append(nEl.Artists, &model.Artist{
				ID:        v[el].ID,
				Name:      v[el].Name,
				CreatedAt: v[el].CreatedAt,
				UpdatedAt: v[el].UpdatedAt,
			})
		}

	}
	if albumSongs := album.Songs(); len(albumSongs) > 0 {
		if nEl.Songs, err = GetSongsModel(r.DB, albumSongs, true); err != nil {
			return nil, errors.New("internal system error")
		}
	}
	genres := album.Genres()
	for ind := range genres {
		nEl.Genres = append(nEl.Genres, &model.MusicGenre{
			ID:   genres[ind].ID,
			Name: genres[ind].Name,
		})
	}
	rt = nEl
	return rt, nil
}

func (r *queryResolver) LastuploadedAlbums(ctx context.Context, pagination *model.PaginationInput) (*model.AlbumResults, error) {
	var err error
	var albums []db.AlbumModel
	var rt []*model.Album
	var usrCtx *middlewares.UserContext
	var results = []map[string]int{}
	var defCount = 100
	var defOffset = 0
	var defPagination = model.PaginationInput{
		Count: &defCount,
		Page:  &defOffset,
	}
	if pagination != nil {
		if pagination.Count != nil {
			defPagination.Count = pagination.Count
		}
		if pagination.Page != nil {
			defPagination.Page = pagination.Page
		}
	}

	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'albums' query : %s\n", err.Error())
		return nil, err
	}
	shares, err := GetSharedAccessUserIDs(usrCtx.ID, r.DB)
	if err != nil {
		log.Errorf("[ERROR] Failed to get shared with : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	if err = r.DB.Prisma.QueryRaw(
		`SELECT COUNT(id) FROM polify."Album" WHERE "ownerId" = ANY($1);`,
		shares,
	).Exec(ctx, &results); err != nil {
		log.Errorf("Error counting albums : %s\n", err.Error())
		return nil, errors.New("server error")
	}
	if albums, err = r.DB.Album.FindMany(
		db.Album.OwnerID.In(shares),
	).
		With(
			db.Album.Artists.Fetch(),
			db.Album.Songs.Fetch(),
			db.Album.Genres.Fetch(),
		).
		OrderBy(db.Album.CreatedAt.Order(db.DESC)).
		Skip(*defPagination.Count * *defPagination.Page).
		Take(*defPagination.Count).
		Exec(ctx); err != nil {
		log.Errorf("Error searching albums : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}
	for el := range albums {
		nEl := &model.Album{
			ID:        albums[el].ID,
			Name:      albums[el].Name,
			CreatedAt: albums[el].CreatedAt,
			UpdatedAt: albums[el].UpdatedAt,
			S3Key:     albums[el].S3Key,
		}
		if v, ok := albums[el].Year(); ok {
			nEl.Year = &v
		}
		if v, ok := albums[el].CoverURL(); ok {
			nEl.CoverURL = &v
		}
		if v, ok := albums[el].Label(); ok {
			nEl.Label = &v
		}
		if v := albums[el].Artists(); len(v) > 0 {
			for el := range v {
				nEl.Artists = append(nEl.Artists, &model.Artist{
					ID:        v[el].ID,
					Name:      v[el].Name,
					CreatedAt: v[el].CreatedAt,
					UpdatedAt: v[el].UpdatedAt,
				})
			}

		}
		if v := albums[el].Songs(); len(v) > 0 {
			for el := range v {
				nSong := &model.Song{
					ID:        v[el].ID,
					Title:     v[el].Title,
					CreatedAt: v[el].CreatedAt,
					UpdatedAt: v[el].UpdatedAt,
					S3Key:     v[el].S3Key,
				}
				if v, ok := v[el].CoverURL(); ok {
					nSong.CoverURL = &v
				}
				nEl.Songs = append(nEl.Songs, nSong)
			}

		}
		genres := albums[el].Genres()
		for ind := range genres {
			nEl.Genres = append(nEl.Genres, &model.MusicGenre{
				ID:   genres[ind].ID,
				Name: genres[ind].Name,
			})
		}
		rt = append(rt, nEl)
	}
	val := results[0]["count"]
	res := model.AlbumResults{
		Total:  &val,
		Albums: rt,
	}
	return &res, nil
}

func (r *queryResolver) LastuploadedSongs(ctx context.Context, pagination *model.PaginationInput) (*model.SongResults, error) {
	var err error
	var songs []db.SongModel
	var rt []*model.Song
	var res *model.SongResults
	var usrCtx *middlewares.UserContext
	var defCount = 100
	var defOffset = 0
	var results = []map[string]int{}
	var defPagination = model.PaginationInput{
		Count: &defCount,
		Page:  &defOffset,
	}
	if pagination != nil {
		if pagination.Count != nil {
			defPagination.Count = pagination.Count
		}
		if pagination.Page != nil {
			defPagination.Page = pagination.Page
		}
	}

	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'lastuploadedsongs' query : %s\n", err.Error())
		return nil, err
	}
	shares, err := GetSharedAccessUserIDs(usrCtx.ID, r.DB)
	if err != nil {
		log.Errorf("[ERROR] Failed to get shared with : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	if err = r.DB.Prisma.QueryRaw(
		`SELECT COUNT(id) FROM polify."Song" WHERE "ownerId" = ANY($1);`,
		shares,
	).Exec(ctx, &results); err != nil {
		log.Errorf("Failed to count song : %s\n", err.Error())
		return nil, errors.New("server error")
	}
	if songs, err = r.DB.Song.FindMany(
		db.Song.OwnerID.In(shares),
		// db.Song.Or(db.Son),

	).
		With(
			db.Song.Album.Fetch(),
			db.Song.Artist.Fetch(),
			db.Song.Genres.Fetch(),
		).
		OrderBy(db.Song.UpdatedAt.Order(db.DESC)).
		Skip(*defPagination.Count * *defPagination.Page).
		Take(*defPagination.Count).
		Exec(ctx); err != nil {
		log.Errorf("Failed to list songs : %s\n", err.Error())

		return nil, errors.New("internal system error")
	}
	if rt, err = GetSongsModel(r.DB, songs, false); err != nil {
		return nil, errors.New("internal system error")
	}
	val := results[0]["count"]
	res = &model.SongResults{
		Total: &val,
		Songs: rt,
	}
	return res, nil
}

func (r *queryResolver) Playlists(ctx context.Context, pagination *model.PaginationInput) ([]*model.Playlist, error) {
	var err error
	var playlists []db.PlaylistModel
	var rt []*model.Playlist
	var usrCtx *middlewares.UserContext
	var defCount = 100
	var defOffset = 0
	var defPagination = model.PaginationInput{
		Count: &defCount,
		Page:  &defOffset,
	}
	if pagination != nil {
		if pagination.Count != nil {
			defPagination.Count = pagination.Count
		}
		if pagination.Page != nil {
			defPagination.Page = pagination.Page
		}
	}
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'playlists' query : %s\n", err.Error())
		return nil, err
	}
	shares, err := GetSharedAccessUserIDs(usrCtx.ID, r.DB)
	if err != nil {
		log.Errorf("[ERROR] Failed to get shared with : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	if playlists, err = r.DB.Playlist.FindMany(
		db.Playlist.CreatorID.In(shares),
	).
		With(
			db.Playlist.Genres.Fetch(),
			db.Playlist.Songs.Fetch().With(
				db.Song.Artist.Fetch(),
				db.Song.Album.Fetch(),
				db.Song.Genres.Fetch(),
				db.Song.S3SignedURL.Fetch(),
			),
		).
		Skip(*defPagination.Count * *defPagination.Page).
		Take(*defPagination.Count).
		Exec(ctx); err != nil {
		log.Errorf("Error searching playlists : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	for el := range playlists {
		nEl := &model.Playlist{
			ID:        playlists[el].ID,
			CreatedAt: playlists[el].CreatedAt,
			UpdatedAt: playlists[el].UpdatedAt,
			Name:      playlists[el].Name,
		}
		if v, ok := playlists[el].CoverURL(); ok {
			nEl.CoverURL = &v
		}
		genres := playlists[el].Genres()
		for i := range genres {
			nEl.Genres = append(nEl.Genres, &model.MusicGenre{
				ID:   genres[i].ID,
				Name: genres[i].Name,
			})
		}
		if v := playlists[el].Songs(); len(v) > 0 {
			if nEl.Songs, err = GetSongsModel(r.DB, v, false); err != nil {
				return nil, errors.New("internal system error")
			}
		}
		rt = append(rt, nEl)
	}
	return rt, nil
}

func (r *queryResolver) Playlist(ctx context.Context, id string) (*model.Playlist, error) {
	var err error
	var playlist *db.PlaylistModel
	var rt *model.Playlist
	var usrCtx *middlewares.UserContext

	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'playlists' query : %s\n", err.Error())
		return nil, err
	}
	if playlist, err = r.DB.Playlist.FindUnique(
		db.Playlist.ID.Equals(id),
	).
		With(
			db.Playlist.Genres.Fetch(),
			db.Playlist.Songs.Fetch().With(
				db.Song.Artist.Fetch(),
				db.Song.Album.Fetch(),
				db.Song.Genres.Fetch(),
				db.Song.S3SignedURL.Fetch(),
			),
		).
		Exec(ctx); err != nil {
		log.Errorf("Error searching playlist : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	rt = &model.Playlist{
		ID:        playlist.ID,
		Name:      playlist.Name,
		CreatedAt: playlist.CreatedAt,
		UpdatedAt: playlist.UpdatedAt,
	}
	if v, ok := playlist.CoverURL(); ok {
		rt.CoverURL = &v
	}
	if v, ok := playlist.Description(); ok {
		rt.Description = &v
	}
	genres := playlist.Genres()
	for i := range genres {
		rt.Genres = append(rt.Genres, &model.MusicGenre{
			ID:   genres[i].ID,
			Name: genres[i].Name,
		})
	}
	if v := playlist.Songs(); len(v) > 0 {
		if rt.Songs, err = GetSongsModel(r.DB, v, true); err != nil {
			return nil, errors.New("internal system error")
		}
	}
	return rt, nil
}

func (r *queryResolver) RandomPlaylist(ctx context.Context, pagination *model.PaginationInput) (*model.Playlist, error) {
	var err error
	var songs []db.SongModel
	var rt *model.Playlist
	var usrCtx *middlewares.UserContext
	var defCount = 100
	var defOffset = 0
	var defPagination = model.PaginationInput{
		Count: &defCount,
		Page:  &defOffset,
	}
	type songTitles struct {
		ID string `sql:"id"`
	}
	if pagination != nil {
		if pagination.Count != nil {
			defPagination.Count = pagination.Count
		}
		if pagination.Page != nil {
			defPagination.Page = pagination.Page
		}
	}
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'randomplaylist' query : %s\n", err.Error())
		return nil, err
	}
	shares, err := GetSharedAccessUserIDs(usrCtx.ID, r.DB)
	if err != nil {
		log.Errorf("[ERROR] Failed to get shared with : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	q := `SELECT id FROM polify."Song" WHERE "Song"."ownerId" = ANY($1) ORDER BY RANDOM() LIMIT $2`
	randomSongs := []songTitles{}
	resSongsIds := []string{}

	if err = r.DB.Prisma.QueryRaw(q, shares, defPagination.Count).Exec(ctx, &randomSongs); err == nil {
		for el := range randomSongs {
			resSongsIds = append(resSongsIds, randomSongs[el].ID)
		}
	} else if err != nil {
		log.Errorf("failed to get random song ids : %s\n", err.Error())
	}
	if songs, err = r.DB.Song.FindMany(
		db.Song.OwnerID.In(shares),
		db.Song.ID.In(resSongsIds),
	).
		With(
			db.Song.Artist.Fetch(),
			db.Song.Album.Fetch(),
			db.Song.Genres.Fetch(),
			db.Song.S3SignedURL.Fetch(),
		).
		Skip(*defPagination.Count * *defPagination.Page).
		Take(*defPagination.Count).
		Exec(ctx); err != nil {
		log.Errorf("Error getting random playlist songs : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}
	desc := fmt.Sprintf("%s playlist.", time.Now().Format(time.RFC3339))
	rt = &model.Playlist{
		ID:          "xxx",
		Name:        sillyname.GenerateStupidName(),
		Description: &desc,
	}
	if len(songs) > 0 {
		if rt.Songs, err = GetSongsModel(r.DB, songs, true); err != nil {
			return nil, errors.New("internal system error")
		}
	}
	return rt, nil
}

func (r *queryResolver) Login(ctx context.Context, name string, password string) (*model.Auth, error) {
	var (
		err                       error
		auth                      *model.Auth
		user                      *db.UserModel
		userDevice                *middlewares.DeviceContext
		accessToken, refreshToken string
		errInvalidCredentials     = errors.New("bad credential, invalid user or password")
	)
	if user, err = r.DB.User.FindUnique(
		db.User.Name.Equals(name),
	).Exec(ctx); err != nil {
		log.Errorf("unknow user '%s' trying to login : %s\n", name, err.Error())
		return nil, errInvalidCredentials
	}
	fmt.Printf("user '%s' trying to login\n", user.Name)
	if err = bcrypt.CompareHashAndPassword([]byte(user.PasswordHash), []byte(password)); err != nil {
		log.Errorf("user '%s' login failed : %s\n", user.Name, err.Error())
		return nil, errInvalidCredentials
	}
	if userDevice = middlewares.DeviceForContext(ctx); userDevice == nil {
		log.Errorf("failed to get device informations\n")
		return nil, errors.New("failed to get device informations")
	}
	if _, err = r.DB.Device.UpsertOne(
		db.Device.DeviceOwnerUseragentUniqueKey(
			db.Device.OwnerID.Equals(user.ID),
			db.Device.UserAgent.Equals(userDevice.UserAgent),
		),
	).Update(
		db.Device.LastConnection.Set(time.Now().Local()),
		db.Device.LastIP.Set(userDevice.IP),
		db.Device.Name.Set(userDevice.Name),
		db.Device.Os.Set(userDevice.OS),
		db.Device.Type.Set(userDevice.Type),
	).Create(
		db.Device.Owner.Link(db.User.ID.Equals(user.ID)),
		db.Device.LastConnection.Set(time.Now().Local()),
		db.Device.UserAgent.Set(userDevice.UserAgent),
		db.Device.LastIP.Set(userDevice.IP),
		db.Device.Name.Set(userDevice.Name),
		db.Device.Os.Set(userDevice.OS),
		db.Device.Type.Set(userDevice.Type),
	).
		Exec(ctx); err != nil {
		log.Errorf("Error saving device : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	jwtWrapper := jwt.Wrapper{
		SecretKey: os.Getenv("JWT_SECRET"),
		Issuer:    "AuthService",
	}
	jwtWrapper.ExpirationHours = 24 // 1 day
	if accessToken, err = jwtWrapper.GenerateToken(user.Name, user.ID); err != nil {
		return nil, err
	}
	jwtWrapper.ExpirationHours = 24 * 15 // 2 Weeks
	if refreshToken, err = jwtWrapper.GenerateToken("NA", user.ID); err != nil {
		return nil, err
	}
	weeksDuration := time.Duration(15 * (24 * time.Hour)) // = refresh token exp
	r.Cache.Set(ctx, user.ID+":"+refreshToken, 0, weeksDuration)
	auth = &model.Auth{
		User: &model.User{
			ID:        user.ID,
			Name:      user.Name,
			CreatedAt: user.CreatedAt,
			UpdatedAt: user.UpdatedAt,
		},
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}
	Ca := middlewares.GetCookieAccess(ctx)
	Ca.SetToken(accessToken, 24*time.Hour)
	Ca.UserId = user.ID
	log.Printf("'%s' is logged with '%s'\n", user.Name, userDevice.UserAgent)
	return auth, nil
}

func (r *queryResolver) Users(ctx context.Context, pagination *model.PaginationInput) (*model.UserResults, error) {
	var (
		err           error
		users         []db.UserModel
		usrsFmt       []*model.UserItem
		results       = []map[string]int{}
		cnt           int
		usrCtx        *middlewares.UserContext
		defCount      = 100
		defOffset     = 0
		defPagination = model.PaginationInput{
			Count: &defCount,
			Page:  &defOffset,
		}
	)
	if pagination != nil {
		if pagination.Count != nil {
			defPagination.Count = pagination.Count
		}
		if pagination.Page != nil {
			defPagination.Page = pagination.Page
		}
	}
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'users' : %s\n", err.Error())
		return nil, err
	}
	if err = r.DB.Prisma.QueryRaw(
		`SELECT COUNT(id) FROM polify."User";`,
	).Exec(ctx, &results); err != nil {
		log.Errorf("Failed to count users : %s\n", err.Error())
		return nil, errors.New("server error")
	}
	if users, err = r.DB.User.FindMany(
		db.User.Not(db.User.ID.Equals(usrCtx.ID)),
	).With(
		db.User.Playlists.Fetch(),
		db.User.Songs.Fetch(),
		db.User.Albums.Fetch(),
	).
		Skip(*defPagination.Count * *defPagination.Page).
		Take(*defPagination.Count).
		Exec(ctx); err != nil {
		log.Errorf("failed to find many users : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	for el := range users {
		nFmt := &model.UserItem{
			ID:        users[el].ID,
			Name:      users[el].Name,
			CreatedAt: users[el].CreatedAt,
			UpdatedAt: users[el].UpdatedAt,
		}
		if ss := users[el].Songs(); len(ss) > 0 {
			v := len(ss) - 1
			nFmt.SongsCount = &v
		}
		if ss := users[el].Albums(); len(ss) > 0 {
			v := len(ss) - 1
			nFmt.AlbumsCount = &v
		}
		if ss := users[el].Playlists(); len(ss) > 0 {
			v := len(ss) - 1
			nFmt.PlaylistsCount = &v
		}
		usrsFmt = append(usrsFmt, nFmt)
	}
	if v, ok := results[0]["count"]; ok {
		cnt = v - 1 // -1 = less current user
	}
	rt := &model.UserResults{
		Total: &cnt,
		Users: usrsFmt,
	}
	return rt, nil
}

func (r *queryResolver) Genres(ctx context.Context, withsongs *bool, pagination *model.PaginationInput) (*model.GenreResults, error) {
	var (
		err           error
		genres        []db.MusicGenreModel
		genresFmt     []*model.MusicGenre
		results       = []map[string]int{}
		cnt           int
		usrCtx        *middlewares.UserContext
		defCount      = 100
		defOffset     = 0
		defPagination = model.PaginationInput{
			Count: &defCount,
			Page:  &defOffset,
		}
	)
	if pagination != nil {
		if pagination.Count != nil {
			defPagination.Count = pagination.Count
		}
		if pagination.Page != nil {
			defPagination.Page = pagination.Page
		}
	}
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'users' : %s\n", err.Error())
		return nil, err
	}
	shares, err := GetSharedAccessUserIDs(usrCtx.ID, r.DB)
	if err != nil {
		log.Errorf("[ERROR] Failed to get shared with : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	if err = r.DB.Prisma.QueryRaw(
		`SELECT COUNT(id) FROM polify."MusicGenre";`,
	).Exec(ctx, &results); err != nil {
		log.Errorf("Failed to count music genres : %s\n", err.Error())
		return nil, errors.New("server error")
	}
	if v, ok := results[0]["count"]; ok {
		cnt = v
	}
	if genres, err = r.DB.MusicGenre.FindMany().
		Skip(*defPagination.Count**defPagination.Page).
		Take(*defPagination.Count).
		With(
			db.MusicGenre.Songs.Fetch(
				db.Song.OwnerID.In(shares),
			),
			db.MusicGenre.Albums.Fetch(
				db.Album.OwnerID.In(shares),
			),
		).
		Exec(ctx); err != nil {
		log.Errorf("Failed to query music genres : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	for i := range genres {
		ngr := model.MusicGenre{
			ID:     genres[i].ID,
			Name:   genres[i].Name,
			Type:   string(genres[i].Type),
			Songs:  []*model.Song{},
			Albums: []*model.Album{},
		}
		songs := genres[i].Songs()
		for el := range songs {
			nSong := model.Song{
				ID:    songs[el].ID,
				Title: songs[el].Title,
			}
			ngr.Songs = append(ngr.Songs, &nSong)
		}
		albums := genres[i].Albums()
		for el := range albums {
			nAlbum := model.Album{
				ID:   albums[el].ID,
				Name: albums[el].Name,
			}
			ngr.Albums = append(ngr.Albums, &nAlbum)
		}
		if (len(ngr.Albums) == 0 && len(ngr.Songs) == 0) && (withsongs != nil && *withsongs) {
			continue
		}
		genresFmt = append(genresFmt, &ngr)
	}
	return &model.GenreResults{
		Total:  &cnt,
		Genres: genresFmt,
	}, nil
}

func (r *queryResolver) Genre(ctx context.Context, id int) (*model.MusicGenre, error) {
	var (
		err    error
		genre  *db.MusicGenreModel
		usrCtx *middlewares.UserContext
	)

	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'users' : %s\n", err.Error())
		return nil, err
	}
	shares, err := GetSharedAccessUserIDs(usrCtx.ID, r.DB)
	if err != nil {
		log.Errorf("[ERROR] Failed to get shared with : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}

	if genre, err = r.DB.MusicGenre.FindUnique(
		db.MusicGenre.ID.Equals(id),
	).
		With(
			db.MusicGenre.Songs.Fetch(
				db.Song.OwnerID.In(shares),
			),
			db.MusicGenre.Albums.Fetch(
				db.Album.OwnerID.In(shares),
			),
		).
		Exec(ctx); err != nil {
		log.Errorf("Failed to query music genres : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	ngr := model.MusicGenre{
		ID:     genre.ID,
		Name:   genre.Name,
		Type:   string(genre.Type),
		Songs:  []*model.Song{},
		Albums: []*model.Album{},
	}
	songs := genre.Songs()
	for el := range songs {
		nSong := model.Song{
			ID:    songs[el].ID,
			Title: songs[el].Title,
		}
		ngr.Songs = append(ngr.Songs, &nSong)
	}
	albums := genre.Albums()
	for el := range albums {
		nAlbum := model.Album{
			ID:   albums[el].ID,
			Name: albums[el].Name,
		}
		ngr.Albums = append(ngr.Albums, &nAlbum)
	}

	return &ngr, nil
}

func (r *queryResolver) Profile(ctx context.Context) (*model.User, error) {
	var (
		err                                       error
		user                                      *db.UserModel
		s3Credential                              *model.S3Credential
		sharedS3Creds                             []*model.S3Credential
		userDevices                               []*model.Device
		askedsharingrequests, receivedsharingreqs []*model.SharingRequest
		usrCtx                                    *middlewares.UserContext
	)
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		err = errors.New("missing auth")
		log.Errorf("Unhautorized access to 'profile' : %s\n", err.Error())
		return nil, err
	}
	if user, err = r.DB.User.FindUnique(
		db.User.ID.Equals(usrCtx.ID)).
		With(
			db.User.Devices.Fetch(),
			db.User.S3Credential.Fetch(),
			db.User.S3SharedCredential.Fetch(),
			db.User.Receivedsharingrequests.Fetch().With(
				db.SharingRequest.AskedBy.Fetch(),
				db.SharingRequest.AskedTo.Fetch(),
			),
			db.User.Askedsharingrequests.Fetch().With(
				db.SharingRequest.AskedBy.Fetch(),
				db.SharingRequest.AskedTo.Fetch(),
			),
		).
		Exec(ctx); err != nil {
		log.Errorf("Failed to query user : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	usrCreds, gotCreds := user.S3Credential()
	if gotCreds {
		s3Credential = &model.S3Credential{
			ID:         usrCreds.ID,
			CreatedAt:  usrCreds.CreatedAt,
			UpdatedAt:  usrCreds.UpdatedAt,
			KeyID:      &usrCreds.KeyID,
			KeySecret:  &usrCreds.KeySecret,
			BucketHost: usrCreds.BucketHost,
			BucketName: usrCreds.BucketName,
		}
		if v, ok := usrCreds.ClientRegion(); ok {
			s3Credential.ClientRegion = &v
		}
		if v, ok := usrCreds.BucketRegion(); ok {
			s3Credential.BucketRegion = &v
		}
	}
	usrSharedS3 := user.S3SharedCredential()
	for el := range usrSharedS3 {
		nshrcred := &model.S3Credential{
			ID:         usrSharedS3[el].ID,
			CreatedAt:  usrSharedS3[el].CreatedAt,
			UpdatedAt:  usrSharedS3[el].UpdatedAt,
			BucketHost: usrSharedS3[el].BucketHost,
			BucketName: usrSharedS3[el].BucketName,
		}
		if usrCtx.IsAdmin {
			nshrcred.KeyID = &usrSharedS3[el].KeyID
			nshrcred.KeySecret = &usrSharedS3[el].KeySecret
		}

		sharedS3Creds = append(sharedS3Creds, nshrcred)
	}
	usrDevs := user.Devices()
	for el := range usrDevs {
		userDevices = append(userDevices, &model.Device{
			ID:             usrDevs[el].ID,
			CreatedAt:      usrDevs[el].CreatedAt,
			UpdatedAt:      usrDevs[el].UpdatedAt,
			LastConnection: usrDevs[el].LastConnection,
			LastIP:         usrDevs[el].LastIP,
			UserAgent:      usrDevs[el].UserAgent,
			Name:           usrDevs[el].Name,
			Os:             usrDevs[el].Os,
			Type:           usrDevs[el].Type,
		})
	}
	sharingreq := user.Askedsharingrequests()
	fmt.Printf("[INFO] Got asked sharing requests len %v\n", len(sharingreq))
	for el := range sharingreq {
		nshar := &model.SharingRequest{
			ID:        sharingreq[el].ID,
			Accepted:  sharingreq[el].Accepted,
			CreatedAt: sharingreq[el].CreatedAt,
			UpdatedAt: sharingreq[el].UpdatedAt,
		}

		nshar.To = &model.User{
			ID:   sharingreq[el].AskedTo().ID,
			Name: sharingreq[el].AskedTo().Name,
		}

		nshar.From = &model.User{
			ID:   sharingreq[el].AskedBy().ID,
			Name: sharingreq[el].AskedBy().Name,
		}

		if v, ok := sharingreq[el].Refused(); ok {
			nshar.Refused = &v
		}
		askedsharingrequests = append(askedsharingrequests, nshar)
	}
	receivedSHaringReqss := user.Receivedsharingrequests()
	fmt.Printf("[INFO] Got received sharing requests len %v\n", len(receivedSHaringReqss))
	for el := range receivedSHaringReqss {
		nshar := &model.SharingRequest{
			ID:        receivedSHaringReqss[el].ID,
			Accepted:  receivedSHaringReqss[el].Accepted,
			CreatedAt: receivedSHaringReqss[el].CreatedAt,
			UpdatedAt: receivedSHaringReqss[el].UpdatedAt,
		}

		nshar.To = &model.User{
			ID:   receivedSHaringReqss[el].AskedTo().ID,
			Name: receivedSHaringReqss[el].AskedTo().Name,
		}
		nshar.From = &model.User{
			ID:   receivedSHaringReqss[el].AskedBy().ID,
			Name: receivedSHaringReqss[el].AskedBy().Name,
		}
		if v, ok := receivedSHaringReqss[el].Refused(); ok {
			nshar.Refused = &v
		}
		receivedsharingreqs = append(receivedsharingreqs, nshar)
	}

	return &model.User{
		ID:                      user.ID,
		CreatedAt:               user.CreatedAt,
		UpdatedAt:               user.UpdatedAt,
		Name:                    user.Name,
		Devices:                 userDevices,
		S3Credential:            s3Credential,
		S3SharedCredential:      sharedS3Creds,
		ReceivedSharingRequests: receivedsharingreqs,
		AskedSharingRequests:    askedsharingrequests,
	}, nil
}

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type queryResolver struct{ *Resolver }
