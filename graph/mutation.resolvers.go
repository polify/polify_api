package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"errors"
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/polify/polify_api/db"
	"gitlab.com/polify/polify_api/logrz"

	"gitlab.com/polify/polify_api/graph/generated"
	"gitlab.com/polify/polify_api/graph/model"
	"gitlab.com/polify/polify_api/middlewares"
	"golang.org/x/crypto/bcrypt"
)

var log = logrz.LL

func (r *mutationResolver) Register(ctx context.Context, input model.NewUser, inviteCode string) (*model.User, error) {
	var (
		err          error
		passwordHash []byte
		userDevice   *middlewares.DeviceContext
		newUser      *db.UserModel
		nDevice      *db.DeviceModel
		invitecode   = os.Getenv("INVITE_CODE")
	)
	if inviteCode == "" || invitecode != inviteCode {
		log.Errorf("Invalid invite code '%s' used", inviteCode)
		return nil, errors.New("invalid or expired invite code")
	}
	if passwordHash, err = bcrypt.GenerateFromPassword(
		[]byte(input.Password),
		bcrypt.DefaultCost,
	); err != nil {
		log.Errorf("Error hashing pwd : %s\n", err.Error())
		return nil, errors.New("bad password")
	}
	if userDevice = middlewares.DeviceForContext(ctx); userDevice == nil {
		return nil, errors.New("failed to get device informations")
	}
	if newUser, err = r.DB.User.CreateOne(
		db.User.Name.Set(input.Name),
		db.User.PasswordHash.Set(string(passwordHash)),
	).Exec(ctx); err != nil {
		log.Errorf(" Error saving user : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}

	if nDevice, err = r.DB.Device.CreateOne(
		db.Device.Owner.Link(db.User.ID.Equals(newUser.ID)),
		db.Device.LastConnection.Set(time.Now().Local()),
		db.Device.UserAgent.Set(userDevice.UserAgent),
		db.Device.LastIP.Set(userDevice.IP),
	).Exec(ctx); err != nil {
		log.Errorf(" Error saving device : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	return &model.User{
		ID:        newUser.ID,
		Name:      newUser.Name,
		CreatedAt: newUser.CreatedAt,
		UpdatedAt: newUser.UpdatedAt,
		Devices: []*model.Device{
			{
				ID:             nDevice.ID,
				CreatedAt:      nDevice.CreatedAt,
				UpdatedAt:      nDevice.UpdatedAt,
				UserAgent:      nDevice.UserAgent,
				LastConnection: nDevice.LastConnection,
				LastIP:         nDevice.LastIP,
			},
		},
	}, nil
}

func (r *mutationResolver) AddS3credential(ctx context.Context, input model.S3CredentialCreateInput) (*model.S3Credential, error) {
	var (
		err           error
		nS3Credential *db.S3CredentialModel
		usrCtx        *middlewares.UserContext
	)
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		return nil, errors.New("missing auth")
	}
	if nS3Credential, err = r.DB.S3Credential.
		CreateOne(
			db.S3Credential.Owner.Link(db.User.ID.Equals(usrCtx.ID)),
			db.S3Credential.BucketHost.Set(input.BucketHost),
			db.S3Credential.BucketName.Set(input.BucketName),
			db.S3Credential.KeyID.Set(input.KeyID),
			db.S3Credential.KeySecret.Set(input.KeySecret),
			db.S3Credential.ClientRegion.SetIfPresent(input.ClientRegion),
			db.S3Credential.BucketRegion.SetIfPresent(input.BucketRegion),
		).
		Exec(ctx); err != nil {
		log.Errorf(" Failed to create bucket : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	resFmt := &model.S3Credential{
		ID:         nS3Credential.ID,
		CreatedAt:  nS3Credential.CreatedAt,
		UpdatedAt:  nS3Credential.UpdatedAt,
		BucketHost: nS3Credential.BucketHost,
		KeyID:      &nS3Credential.KeyID,
		KeySecret:  &nS3Credential.KeySecret,
	}
	if v, ok := nS3Credential.ClientRegion(); ok {
		resFmt.ClientRegion = &v

	}
	return resFmt, nil
}

func (r *mutationResolver) UpdateS3credential(ctx context.Context, id string, input model.S3CredentialUpdateInput) (*model.S3Credential, error) {
	var (
		err           error
		nS3Credential *db.S3CredentialModel
		usrCtx        *middlewares.UserContext
	)
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		return nil, errors.New("missing auth")
	}
	if nS3Credential, err = r.DB.S3Credential.
		FindUnique(
			db.S3Credential.OwnerID.Equals(usrCtx.ID),
		).
		Update(
			db.S3Credential.BucketHost.SetIfPresent(input.BucketHost),
			db.S3Credential.BucketName.SetIfPresent(input.BucketName),
			db.S3Credential.KeyID.SetIfPresent(input.KeyID),
			db.S3Credential.KeySecret.SetIfPresent(input.KeySecret),
			db.S3Credential.ClientRegion.SetIfPresent(input.ClientRegion),
			db.S3Credential.BucketRegion.SetIfPresent(input.BucketRegion),
		).
		Exec(ctx); err != nil {
		log.Errorf("Failed to update bucket : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	rt := &model.S3Credential{
		ID:         nS3Credential.ID,
		CreatedAt:  nS3Credential.CreatedAt,
		UpdatedAt:  nS3Credential.UpdatedAt,
		BucketHost: nS3Credential.BucketHost,
		KeyID:      &nS3Credential.KeyID,
		KeySecret:  &nS3Credential.KeySecret,
		BucketName: nS3Credential.BucketName,
	}
	if v, ok := nS3Credential.BucketRegion(); ok {
		rt.BucketRegion = &v
	}
	if v, ok := nS3Credential.ClientRegion(); ok {
		rt.ClientRegion = &v
	}
	return rt, nil
}

func (r *mutationResolver) Createsharingrequest(ctx context.Context, toUserID string) (*model.SharingRequest, error) {
	var (
		err             error
		nSharingRequest *db.SharingRequestModel
		usrCtx          *middlewares.UserContext
	)
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		return nil, errors.New("missing auth")
	}
	if nSharingRequest, err = r.DB.SharingRequest.CreateOne(
		db.SharingRequest.AskedTo.Link(db.User.ID.Equals(toUserID)),
		db.SharingRequest.AskedBy.Link(db.User.ID.Equals(usrCtx.ID)),
		// db.SharingRequest.FromUserID.Set(usrCtx.ID),
	).With(
		db.SharingRequest.AskedTo.Fetch(),
		db.SharingRequest.AskedBy.Fetch(),
	).
		Exec(ctx); err != nil && !strings.Contains(err.Error(), "UniqueConstraintViolation") {
		log.Errorf("failed to create sharing request : %s\n", err.Error())
		return nil, errors.New("internal server error")
	} else if err != nil && strings.Contains(err.Error(), "UniqueConstraintViolation") {
		log.Errorf("failed to create sharing request : %s\n", err.Error())
		return nil, errors.New("already asked")
	}
	res := &model.SharingRequest{
		ID:       nSharingRequest.ID,
		Accepted: nSharingRequest.Accepted,
		// Refused: nSharingRequest.Refused,
		CreatedAt: nSharingRequest.CreatedAt,
		UpdatedAt: nSharingRequest.UpdatedAt,
	}
	res.From = &model.User{
		ID:   nSharingRequest.AskedBy().ID,
		Name: nSharingRequest.AskedBy().Name,
	}

	res.To = &model.User{
		ID:   nSharingRequest.AskedTo().ID,
		Name: nSharingRequest.AskedTo().Name,
	}

	return res, nil
}

func (r *mutationResolver) Updatesharingrequest(ctx context.Context, id string, accept bool, refuse bool) (*model.SharingRequest, error) {
	var (
		err                   error
		updatedSharingRequest *db.SharingRequestModel
		usrCtx                *middlewares.UserContext
	)
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		return nil, errors.New("missing auth")
	}
	if accept && refuse {
		log.Error("invalid update sharing request , both accept and refuse are true")
		return nil, errors.New("invalid update sharing request , both accept and refuse are true")
	}
	if updatedSharingRequest, err = r.DB.SharingRequest.FindUnique(
		db.SharingRequest.ID.Equals(id),
	).
		With(
			db.SharingRequest.AskedTo.Fetch(),
			db.SharingRequest.AskedBy.Fetch(),
		).
		Update(
			db.SharingRequest.Accepted.Set(accept),
			db.SharingRequest.Refused.Set(refuse),
		).Exec(ctx); err != nil {
		log.Errorf("failed to update sharing request : %s\n", err.Error())
		return nil, errors.New("internal server error")
	}
	rt := &model.SharingRequest{
		ID:        updatedSharingRequest.ID,
		Accepted:  updatedSharingRequest.Accepted,
		CreatedAt: updatedSharingRequest.CreatedAt,
		UpdatedAt: updatedSharingRequest.UpdatedAt,
	}
	if v, ok := updatedSharingRequest.Refused(); ok {
		rt.Refused = &v
	}
	rt.To = &model.User{
		ID:   updatedSharingRequest.AskedTo().ID,
		Name: updatedSharingRequest.AskedTo().Name,
	}

	rt.From = &model.User{
		ID:   updatedSharingRequest.AskedBy().ID,
		Name: updatedSharingRequest.AskedBy().Name,
	}
	if accept {
		if _, err = r.DB.S3Credential.FindUnique(
			db.S3Credential.OwnerID.Equals(usrCtx.ID),
		).Update(
			db.S3Credential.SharedWith.Link(
				db.User.ID.Equals(updatedSharingRequest.AskedBy().ID),
			),
		).Exec(ctx); err != nil {
			log.Errorf("failed to update s3credential : %s\n", err.Error())
			return nil, errors.New("internal server error")
		}

	}

	return rt, nil
}

func (r *mutationResolver) Deletedevice(ctx context.Context, id string) (bool, error) {
	var (
		err      error
		ok       bool
		ddDevice *db.DeviceModel
		usrCtx   *middlewares.UserContext
	)
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		return ok, errors.New("missing auth")
	}
	if ddDevice, err = r.DB.Device.FindUnique(
		db.Device.ID.Equals(id),
	).Exec(ctx); err != nil {
		log.Errorf("failed to find device : %s\n", err.Error())
		return ok, errors.New("internal server error")
	}
	if ddDevice.OwnerID != usrCtx.ID && !usrCtx.IsAdmin {
		log.Errorf("Non admin user '%s' (id '%s') trying to remove another user device\n", usrCtx.Name, usrCtx.ID)
		return ok, errors.New("internal server error")
	}
	if _, err = r.DB.Device.FindUnique(db.Device.ID.Equals(id)).
		Delete().
		Exec(ctx); err != nil {
		log.Errorf("failed to remove device : %s\n", err.Error())
		return ok, errors.New("internal server error")
	}
	ok = true
	return ok, nil
}

func (r *mutationResolver) Deleteplaylist(ctx context.Context, id string) (bool, error) {
	var (
		err        error
		ok         bool
		ddPlaylist *db.PlaylistModel
		usrCtx     *middlewares.UserContext
	)
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		return ok, errors.New("missing auth")
	}
	if ddPlaylist, err = r.DB.Playlist.FindUnique(
		db.Playlist.ID.Equals(id),
	).Exec(ctx); err != nil {
		log.Errorf("failed to find playlist : %s\n", err.Error())
		return ok, errors.New("internal server error")
	}
	if ddPlaylist.CreatorID != usrCtx.ID && !usrCtx.IsAdmin {
		log.Errorf("Non admin user '%s' (id '%s') trying to remove another user playlist\n", usrCtx.Name, usrCtx.ID)
		return ok, errors.New("internal server error")
	}
	if _, err = r.DB.Playlist.FindUnique(db.Playlist.ID.Equals(id)).
		Delete().
		Exec(ctx); err != nil {
		log.Errorf("failed to remove playlist : %s\n", err.Error())
		return ok, errors.New("internal server error")
	}
	ok = true
	return ok, nil
}

func (r *mutationResolver) Createplaylist(ctx context.Context, input model.ItemsInput) (*model.Playlist, error) {
	var err error
	var pl *db.PlaylistModel
	var plSong []db.SongWhereParam
	var usrCtx *middlewares.UserContext
	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		return nil, errors.New("missing auth")
	}
	for el := range input.Ids {
		plSong = append(plSong, db.Song.ID.Equals(input.Ids[el]))
	}
	if input.CollectionName == nil {
		return nil, errors.New("missing playlist name")
	}
	if pl, err = r.DB.Playlist.UpsertOne(
		db.Playlist.PlaylistOwnerNameUniqueKey(
			db.Playlist.CreatorID.Equals(usrCtx.ID),
			db.Playlist.Name.EqualsIfPresent(input.CollectionName),
		),
	).Create(
		db.Playlist.Creator.Link(db.User.ID.Equals(usrCtx.ID)),
		db.Playlist.Name.Set(*input.CollectionName),
		db.Playlist.Songs.Link(
			plSong...,
		),
	).Update(
		db.Playlist.Songs.Link(
			plSong...,
		),
	).
		Exec(ctx); err != nil {
		log.Errorf("Failed to upsert playlist : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}
	if pl, err = r.DB.Playlist.FindUnique(
		db.Playlist.ID.Equals(pl.ID),
	).
		With(
			db.Playlist.Genres.Fetch(),
			db.Playlist.Songs.Fetch().With(
				db.Song.Artist.Fetch(),
				db.Song.Album.Fetch(),
				db.Song.S3SignedURL.Fetch(),
				db.Song.Genres.Fetch(),
			),
		).
		Exec(ctx); err != nil {
		log.Errorf("Error getting playlist : %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	fmt.Printf("[INFO] Got playlist %+v\n", pl)
	rt := &model.Playlist{
		ID:        pl.ID,
		CreatedAt: pl.CreatedAt,
		UpdatedAt: pl.UpdatedAt,
		Name:      pl.Name,
	}
	if v, ok := pl.CoverURL(); ok {
		rt.CoverURL = &v
	}
	genres := pl.Genres()
	for i := range genres {
		rt.Genres = append(rt.Genres, &model.MusicGenre{
			ID:   genres[i].ID,
			Name: genres[i].Name,
		})
	}
	if v := pl.Songs(); len(v) > 0 {
		if rt.Songs, err = GetSongsModel(r.DB, v, true); err != nil {
			return nil, errors.New("internal system error")
		}
	}
	return rt, nil
}

func (r *mutationResolver) Updateplaylist(ctx context.Context, id string, input model.ItemsInput) (*model.Playlist, error) {
	var err error
	var pl *db.PlaylistModel
	var usrCtx *middlewares.UserContext
	var oldplSongs, newplSongs []db.SongWhereParam
	// var oldplSongIds, newplSongIds = map[string]bool{}, map[string]bool{}

	if usrCtx = middlewares.UserForContext(ctx); usrCtx == nil {
		return nil, errors.New("missing auth")
	}
	for el := range input.Ids {
		// newplSongIds[input.Ids[el]] = true
		newplSongs = append(newplSongs, db.Song.ID.Equals(input.Ids[el]))
	}
	if pl, err = r.DB.Playlist.FindUnique(
		db.Playlist.ID.Equals(id),
	).With(
		db.Playlist.Songs.Fetch(),
		db.Playlist.Creator.Fetch(),
	).
		Exec(ctx); err != nil {
		log.Errorf("failed to upsert playlist : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}
	if pl.Creator().ID != usrCtx.ID {
		log.Errorf("%s cannot modify %s playlist : %s\n", usrCtx.Name, pl.Creator().Name)
		return nil, errors.New("internal system error")
	}

	var oldSongs = pl.Songs()
	for el := range oldSongs {
		// oldplSongIds[oldSongs[el].ID] = true
		oldplSongs = append(oldplSongs, db.Song.ID.Equals(oldSongs[el].ID))
	}

	if pl, err = r.DB.Playlist.FindUnique(
		db.Playlist.ID.Equals(pl.ID),
	).
		Update(
			db.Playlist.Songs.Unlink(
				oldplSongs...,
			),
		).
		Exec(ctx); err != nil {
		log.Errorf("failed to upsert playlist : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}
	if pl, err = r.DB.Playlist.FindUnique(
		db.Playlist.ID.Equals(pl.ID),
	).
		Update(
			db.Playlist.Songs.Link(
				newplSongs...,
			),
		).
		Exec(ctx); err != nil {
		log.Errorf("failed to upsert playlist : %s\n",
			err.Error())
		return nil, errors.New("internal system error")
	}
	rt := &model.Playlist{
		ID:        pl.ID,
		CreatedAt: pl.CreatedAt,
		UpdatedAt: pl.UpdatedAt,
		Name:      pl.Name,
	}
	return rt, nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

type mutationResolver struct{ *Resolver }
