#!/usr/bin/env bash
source ".$stage.env"
echo "[INFO] Will populate heroku app '$HEROKU_APP_NAME' environment with .$stage.env values"
sed '1,2d' ".$stage.env" | xargs npx heroku config:set -a ${HEROKU_APP_NAME}
# Above sed is here to avoid HEROKU_APP** and DATABASE_URL keys conflict with Heroku