############################
# STEP 1 build executable binary
############################
FROM docker.io/library/golang:alpine AS builder
# Install git to get go deps
RUN apk update && apk add --no-cache curl gcc musl-dev git ca-certificates tzdata && update-ca-certificates
# Create appuser.
# ENV USER=appuser
# ENV UID=10001 
# RUN adduser \    
#     --disabled-password \    
#     --gecos "" \    
#     --home "/nonexistent" \    
#     --shell "/sbin/nologin" \    
#     --no-create-home \    
#     --uid "${UID}" \    
#     "${USER}"
# Create build dir
WORKDIR /build
# COPY go.mod and go.sum files to the workspace
COPY go.* ./
# Caching vendored deps
RUN go mod download
# Check their integrity
RUN go mod verify
# COPY the source code as the last step
COPY . .
# Dirty hack to remove the incorrect arch query engine (having both cause issues)
RUN if [ $(uname -m) == "x86_64" ]; then rm db/query-engine-linux-arm64-openssl-1.1.x_gen.go; else rm db/query-engine-debian-openssl-1.1.x_gen.go; fi
RUN ls -la db/
# Build the binary (dynamically linked).
RUN go build -ldflags="-w -s" -o bin/server ./cmd/
RUN echo "[INFO] Successfully build for $(uname -m)"
ARG TARGETPLATFORM
ARG BUILDPLATFORM
RUN echo "I was built on a platform running on $BUILDPLATFORM, building this image for $TARGETPLATFORM" 
############################
# STEP 2 build a small image
############################
FROM scratch
WORKDIR /go
# Import missing files in scratch image like the user and group files from the builder.
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
# COPY --from=builder /etc/passwd /etc/passwd
# COPY --from=builder /etc/group /etc/group
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
# Copy dynamic lib
COPY --from=builder /lib/*.so.1 /lib/
ENV LD_LIBRARY_PATH=/lib/    
# Copy our static executable.
COPY --from=builder /build/bin/server /go/bin/server
# Fix dir permission
# RUN chown -R appuser:appuser /tmp
# Use an unprivileged user.
# USER appuser:appuser
# Run the server binary.
ENTRYPOINT ["/go/bin/server"]
