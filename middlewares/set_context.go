package middlewares

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"strings"

	"gitlab.com/polify/polify_api/db"
	"gitlab.com/polify/polify_api/jwt"

	"github.com/gin-gonic/gin"
	"github.com/labstack/echo/v4"
	ua "github.com/mileusna/useragent"
	"github.com/tomasen/realip"

	"github.com/99designs/gqlgen/graphql/handler/transport"
)

// A private key for context that only this package can access. This is important
// to prevent collisions between different context uses
var userCtxKey = &contextKey{"user"}
var deviceCtxKey = &contextKey{"device"}

type contextKey struct {
	name string
}

// A stand-in for our database backed user object
type UserContext struct {
	ID      string
	Name    string
	IsAdmin bool
}

type DeviceContext struct {
	IP        string
	UserAgent string
	Type      string
	Name      string
	OS        string
	// MacAddress string
}

// InitFunc is the set context for websocket
func InitFunc(ctx context.Context, initPayload transport.InitPayload) (context.Context, error) {
	var err error
	var authString string
	fmt.Printf("[INFO] Subscription init func : %+v\n", initPayload.Authorization())
	if authString = initPayload.Authorization(); authString != "" {
		var claims *jwt.Claim
		jwtWrapper := jwt.Wrapper{
			SecretKey:       os.Getenv("JWT_SECRET"),
			Issuer:          "AuthService",
			ExpirationHours: 24,
		}
		if claims, err = jwtWrapper.ValidateToken(authString); err != nil {
			return ctx, err
		}
		userCtx := &UserContext{
			Name:    claims.UserName,
			ID:      claims.UserID,
			IsAdmin: false,
		}
		ctx = context.WithValue(ctx, userCtxKey, userCtx)
	}
	// and call the next with our new context
	return ctx, nil
}

func SetContextGin(db *db.PrismaClient) gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			err             error
			deviceName      string
			deviceType      string
			deviceOs        string
			deviceOsVersion string
			r               = c.Request
			clientIP        = realip.FromRequest(c.Request)
			contextDevice   = &DeviceContext{
				UserAgent: r.UserAgent(),
				IP:        clientIP,
			}
		)

		if strings.Contains(strings.ToLower(r.UserAgent()), "eclektikplayer") {
			deviceName = "EclektikPlayer"
			foo := strings.Split(r.UserAgent(), ";")
			if len(foo) > 1 {
				deviceOs = foo[1]
				deviceOsVersion = foo[2][:len(foo[2])-2]
				a := strings.ToLower(deviceOs)
				switch {
				case strings.Contains(a, "android"):
					deviceType = "Mobile"
				case strings.Contains(a, "ios"):
					deviceType = "Mobile"
				case strings.Contains(a, "windows"):
					deviceType = "Desktop"
				case strings.Contains(a, "linux"):
					deviceType = "Desktop"
				}
			}
		} else {
			ua := ua.Parse(r.UserAgent())
			deviceName = ua.Name
			deviceOs = ua.OS
			deviceOsVersion = ua.OSVersion
			switch {
			case ua.Desktop:
				deviceType = "Desktop"
			case ua.Mobile:
				deviceType = "Mobile"
			case ua.Tablet:
				deviceType = "Tablet"
			case ua.Bot:
				deviceType = "Bot"
			default:
				deviceType = "Unknown"
			}
		}
		contextDevice.Name = deviceName
		contextDevice.Type = deviceType
		contextDevice.OS = deviceOs + "-" + deviceOsVersion
		// fmt.Printf("[INFO] Device : %+v\n", contextDevice)
		// put it in context
		ctx := context.WithValue(r.Context(), deviceCtxKey, contextDevice)
		var authString = r.Header.Get("Authorization")
		if authString == "" {
			acook, err := r.Cookie(cookieName)
			if err == nil && acook != nil {
				authString = acook.Value
			} else if err != nil && err != http.ErrNoCookie {
				fmt.Printf("[INFO] Trouble getting cookie : %s\n", err.Error())
			}
		}
		if authString != "" {
			var claims *jwt.Claim
			jwtWrapper := jwt.Wrapper{
				SecretKey:       os.Getenv("JWT_SECRET"),
				Issuer:          "AuthService",
				ExpirationHours: 24,
			}
			if claims, err = jwtWrapper.ValidateToken(authString); err != nil {
				c.Request = r.WithContext(ctx)
				// next.ServeHTTP(w, r)
				c.Next()
				return
			}
			userCtx := &UserContext{
				Name:    claims.UserName,
				ID:      claims.UserID,
				IsAdmin: false,
			}
			ctx = context.WithValue(ctx, userCtxKey, userCtx)
		}
		// and call the next with our new context
		c.Request = r.WithContext(ctx)
		// next.ServeHTTP(w, r)
		c.Next()
	}
}

func SetContextEcho(db *db.PrismaClient) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			var (
				err             error
				deviceName      string
				deviceType      string
				deviceOs        string
				deviceOsVersion string
				r               = c.Request()
				clientIP        = realip.FromRequest(c.Request())
				contextDevice   = &DeviceContext{
					UserAgent: r.UserAgent(),
					IP:        clientIP,
				}
				contextUser = &UserContext{}
			)

			if strings.Contains(strings.ToLower(r.UserAgent()), "eclektikplayer") {
				deviceName = "EclektikPlayer"
				foo := strings.Split(r.UserAgent(), ";")
				if len(foo) > 1 {
					deviceOs = foo[1]
					deviceOsVersion = foo[2][:len(foo[2])-2]
					a := strings.ToLower(deviceOs)
					switch {
					case strings.Contains(a, "android"):
						deviceType = "Mobile"
					case strings.Contains(a, "ios"):
						deviceType = "Mobile"
					case strings.Contains(a, "windows"):
						deviceType = "Desktop"
					case strings.Contains(a, "linux"):
						deviceType = "Desktop"
					}
				}
			} else {
				ua := ua.Parse(r.UserAgent())
				deviceName = ua.Name
				deviceOs = ua.OS
				deviceOsVersion = ua.OSVersion
				switch {
				case ua.Desktop:
					deviceType = "Desktop"
				case ua.Mobile:
					deviceType = "Mobile"
				case ua.Tablet:
					deviceType = "Tablet"
				case ua.Bot:
					deviceType = "Bot"
				default:
					deviceType = "Unknown"
				}
			}
			contextDevice.Name = deviceName
			contextDevice.Type = deviceType
			contextDevice.OS = deviceOs + "-" + deviceOsVersion
			c.Set("device", contextDevice)
			ctx := context.WithValue(r.Context(), deviceCtxKey, contextDevice)
			var authString = r.Header.Get("Authorization")
			if authString == "" {
				acook, err := r.Cookie(cookieName)
				if err == nil && acook != nil {
					authString = acook.Value
				} else if err != nil && err != http.ErrNoCookie {
					fmt.Printf("[INFO] Trouble getting cookie : %s\n", err.Error())
				}
			}
			if authString != "" {
				var claims *jwt.Claim
				jwtWrapper := jwt.Wrapper{
					SecretKey:       os.Getenv("JWT_SECRET"),
					Issuer:          "AuthService",
					ExpirationHours: 24,
				}
				if claims, err = jwtWrapper.ValidateToken(authString); err != nil {
					return next(c)
				}

				contextUser.Name = claims.UserName
				contextUser.ID = claims.UserID
				contextUser.IsAdmin = false
				ctx = context.WithValue(ctx, userCtxKey, contextUser)
				c.Set("user", contextUser)

			}
			c.SetRequest(r.WithContext(ctx))
			return next(c)
		}
	}
}

// Middleware decodes the share session cookie and packs the session into context
func SetContextHttp(db *db.PrismaClient) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			var (
				err             error
				authString      string
				deviceName      string
				deviceType      string
				deviceOs        string
				deviceOsVersion string
				clientIP        = realip.FromRequest(r)
				contextDevice   = &DeviceContext{
					UserAgent: r.UserAgent(),
					IP:        clientIP,
				}
			)

			if strings.Contains(strings.ToLower(r.UserAgent()), "eclektikplayer") {
				deviceName = "EclektikPlayer"
				foo := strings.Split(r.UserAgent(), ";")
				if len(foo) > 1 {
					deviceOs = foo[1]
					deviceOsVersion = foo[2][:len(foo[2])-2]
					a := strings.ToLower(deviceOs)
					switch {
					case strings.Contains(a, "android"):
						deviceType = "Android"
					case strings.Contains(a, "ios"):
						deviceType = "iOS"
					case strings.Contains(a, "windows"):
						deviceType = "Windows"
					case strings.Contains(a, "linux"):
						deviceType = "Linux"
					}
				}
			} else {
				ua := ua.Parse(r.UserAgent())
				deviceName = ua.Name
				deviceOs = ua.OS
				deviceOsVersion = ua.OSVersion
				switch {
				case ua.Desktop:
					deviceType = "Desktop"
				case ua.Mobile:
					deviceType = "Mobile"
				case ua.Tablet:
					deviceType = "Tablet"
				case ua.Bot:
					deviceType = "Bot"
				default:
					deviceType = "Unknown"
				}
			}
			contextDevice.Name = deviceName
			contextDevice.Type = deviceType
			contextDevice.OS = deviceOs + "-" + deviceOsVersion
			// fmt.Printf("[INFO] Device : %+v\n", contextDevice)
			// put it in context
			ctx := context.WithValue(r.Context(), deviceCtxKey, contextDevice)
			if authString = r.Header.Get("Authorization"); authString != "" {
				var claims *jwt.Claim
				jwtWrapper := jwt.Wrapper{
					SecretKey:       os.Getenv("JWT_SECRET"),
					Issuer:          "AuthService",
					ExpirationHours: 24,
				}
				if claims, err = jwtWrapper.ValidateToken(authString); err != nil {
					r = r.WithContext(ctx)
					next.ServeHTTP(w, r)
					return
				}
				userCtx := &UserContext{
					Name:    claims.UserName,
					ID:      claims.UserID,
					IsAdmin: false,
				}
				ctx = context.WithValue(ctx, userCtxKey, userCtx)
			}
			// and call the next with our new context
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}

// UserForContext finds the user from the context. REQUIRES Middleware to have run.
func UserForContext(ctx context.Context) *UserContext {
	raw, _ := ctx.Value(userCtxKey).(*UserContext)
	return raw
}

// UserForEchoContext finds the user from the context. REQUIRES Middleware to have run.
func UserForEchoContext(c echo.Context) *UserContext {
	cc, _ := c.Get("user").(*UserContext)
	return cc
}

// DeviceForContext finds the user from the context. REQUIRES Middleware to have run.
func DeviceForContext(ctx context.Context) *DeviceContext {
	raw, _ := ctx.Value(deviceCtxKey).(*DeviceContext)
	return raw
}

// DeviceForEchoContext finds the user from the context. REQUIRES Middleware to have run.
func DeviceForEchoContext(ctx echo.Context) *DeviceContext {
	cc, _ := ctx.Get("device").(*DeviceContext)
	return cc
}
