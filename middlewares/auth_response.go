package middlewares

import (
	"context"
	"errors"
	"net/http"
	"os"
	"time"

	"gitlab.com/polify/polify_api/jwt"

	"github.com/gin-gonic/gin"
	"github.com/labstack/echo/v4"
)

var cookieAccessKeyCtx = &contextKey{"cookie"}

type CookieAccess struct {
	Writer     http.ResponseWriter
	UserId     string
	IsLoggedIn bool
}

const cookieName = "auth"

// method to write cookie
func (c *CookieAccess) SetToken(token string, exp time.Duration) {
	http.SetCookie(c.Writer, &http.Cookie{
		Name:     cookieName,
		Value:    token,
		HttpOnly: true,
		Path:     "/",
		Secure:   true,
		SameSite: http.SameSiteNoneMode,
		Expires:  time.Now().Add(exp),
	})
}

func extractUserId(ctx *gin.Context) (string, error) {
	c, err := ctx.Request.Cookie(cookieName)
	if err != nil {
		return "", errors.New("there is no token in cookies")
	}
	var claims *jwt.Claim
	jwtWrapper := jwt.Wrapper{
		SecretKey:       os.Getenv("JWT_SECRET"),
		Issuer:          "AuthService",
		ExpirationHours: 24,
	}
	claims, err = jwtWrapper.ValidateToken(c.Value)
	if err != nil {
		return "", err
	}
	return claims.UserID, nil
}

func extractUserIdEcho(ctx echo.Context) (string, error) {
	c, err := ctx.Cookie(cookieName)
	if err != nil {
		return "", errors.New("there is no token in cookies")
	}
	var claims *jwt.Claim
	jwtWrapper := jwt.Wrapper{
		SecretKey:       os.Getenv("JWT_SECRET"),
		Issuer:          "AuthService",
		ExpirationHours: 24,
	}
	claims, err = jwtWrapper.ValidateToken(c.Value)
	if err != nil {
		return "", err
	}
	return claims.UserID, nil
}

func setValInGinCtx(ctx *gin.Context, val interface{}) {
	newCtx := context.WithValue(ctx.Request.Context(), cookieAccessKeyCtx, val)
	ctx.Request = ctx.Request.WithContext(newCtx)
}

func setValInEchoCtx(ctx echo.Context, val interface{}) {
	newCtx := context.WithValue(ctx.Request().Context(), cookieAccessKeyCtx, val)
	ctx.SetRequest(ctx.Request().WithContext(newCtx))
}

func GetCookieAccess(ctx context.Context) *CookieAccess {
	return ctx.Value(cookieAccessKeyCtx).(*CookieAccess)
}

func SetAuthCookieGin() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		cookieA := CookieAccess{
			Writer: ctx.Writer,
		}

		// &cookieA is a pointer so any changes in future is changing cookieA is context
		setValInGinCtx(ctx, &cookieA)

		userId, err := extractUserId(ctx)
		if err != nil {
			cookieA.IsLoggedIn = false
			ctx.Next()
			return
		}

		cookieA.UserId = userId
		cookieA.IsLoggedIn = true

		// calling the actual resolver
		ctx.Next()
		// here will execute after resolver and all other middlewares was called
		// so &cookieA is safe from garbage collector
	}
}

func SetAuthCookieEcho(next echo.HandlerFunc) echo.HandlerFunc {
	return func(ctx echo.Context) error {

		cookieA := CookieAccess{
			Writer: ctx.Response().Writer,
		}

		// &cookieA is a pointer so any changes in future is changing cookieA is context
		ctx.Set(cookieAccessKeyCtx.name, &cookieA)
		setValInEchoCtx(ctx, &cookieA)

		userId, err := extractUserIdEcho(ctx)
		if err != nil {
			cookieA.IsLoggedIn = false
			return next(ctx)
		}

		cookieA.UserId = userId
		cookieA.IsLoggedIn = true

		// calling the actual resolver
		return next(ctx)
		// here will execute after resolver and all other middlewares was called
		// so &cookieA is safe from garbage collector
	}
}
