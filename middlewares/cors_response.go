package middlewares

import "net/http"

// SetupCORSResponse set header for cors validation on web browsers
func SetupCORSResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

// CORSHandler is here to catch preflight requests
func CORSHandler(w http.ResponseWriter, req *http.Request) {
	SetupCORSResponse(&w, req)
}
