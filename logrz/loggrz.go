package logrz

import (
	"os"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/polify/polify_api/loghook"
	"gitlab.com/polify/polify_api/loghook/promhook"
)

var LL = logrus.New()

func Init() {
	hook, err := loghook.NewTelegramHook(
		os.Getenv("TELEGRAM_APPNAME")+"-"+os.Getenv("STAGE"),
		os.Getenv("TELEGRAM_TOKEN"),
		os.Getenv("TELEGRAM_USER"),
		loghook.WithAsync(false),
		loghook.WithTimeout(30*time.Second),
	)
	if err != nil {
		LL.Fatalf("failed to create Telegram hook: %s", err)
	}
	LL.AddHook(hook)
	nhook, err := promhook.NewHook(&promhook.Config{
		URL: os.Getenv("LOKI_ENDPOINT"),
		// LevelName: logrus.InfoLevel.String(),
		Labels: map[string]string{
			"application": "polify-api",
		},
		BatchWait: 3 * time.Second,
	})
	if err != nil {
		LL.Fatalf("failed to create Loki hook: %s", err)
	}
	LL.AddHook(nhook)
}
