package polify

import (
	"context"
	"errors"
	"os"
	"strings"

	"gitlab.com/polify/polify_api/db"
	"gitlab.com/polify/polify_api/graph/model"

	"golang.org/x/crypto/bcrypt"
)

func UserSeed(username, password string, cli *db.PrismaClient) (nUser *model.User, err error) {
	var (
		passwordHash []byte
		newUser      *db.UserModel
	)
	if passwordHash, err = bcrypt.GenerateFromPassword(
		[]byte(password),
		bcrypt.DefaultCost,
	); err != nil {
		Log.Errorf("trouble hashing pwd: %s\n", err.Error())
		return nil, errors.New("bad password")
	}
	if newUser, err = cli.User.UpsertOne(
		db.User.Name.Equals(username),
	).Create(
		db.User.Name.Set(username),
		db.User.PasswordHash.Set(string(passwordHash)),
		db.User.Role.Set(db.RoleADMIN),
	).Update(
		db.User.PasswordHash.Set(string(passwordHash)),
		db.User.Role.Set(db.RoleADMIN),
	).
		Exec(context.Background()); err != nil {
		Log.Errorf("failed to upsert user: %s\n", err.Error())
		return nil, errors.New("internal system error")
	}
	Log.Infof("[SEED] Admin user name '%s' id '%s' is set\n", newUser.Name, newUser.ID)
	return &model.User{
		ID:        newUser.ID,
		Name:      newUser.Name,
		CreatedAt: newUser.CreatedAt,
		UpdatedAt: newUser.UpdatedAt,
	}, nil
}

func MusicGenreSeed(cli *db.PrismaClient) (err error) {
	var allGenres = []model.MusicGenre{}
	allGenres = append(allGenres, model.ID3Genres...)
	allGenres = append(allGenres, model.WinampExtension...)
	Log.Infof("[SEED] Will seed %v predefined music genres\n",
		len(allGenres))
	for _, v := range allGenres {
		var ngenre *db.MusicGenreModel
		var genreType db.MusicGenreType
		switch v.Type {
		case string(db.MusicGenreTypeID3):
			genreType = db.MusicGenreTypeID3
		case string(db.MusicGenreTypeWINAMP):
			genreType = db.MusicGenreTypeWINAMP
		default:
			genreType = db.MusicGenreTypeCUSTOM
		}
		if ngenre, err = cli.MusicGenre.UpsertOne(
			db.MusicGenre.Name.Equals(v.Name),
		).Create(
			db.MusicGenre.Name.Set(v.Name),
			db.MusicGenre.Type.Set(genreType),
		).Update(
			db.MusicGenre.Type.Set(genreType),
		).Exec(context.Background()); err != nil {
			return err
		}
		Log.Infof("[SEED] Created or updated music genre '%s' id '%v' (%v)\n",
			v.Name, ngenre.ID, string(genreType))
	}
	return nil
}

func MustSeed(cli *db.PrismaClient) error {
	var err error
	var toSeed = os.Getenv("SEED")
	if _, err = UserSeed(
		os.Getenv("POLIFY_SEED_USER"),
		os.Getenv("POLIFY_SEED_PASS"),
		cli,
	); err != nil {
		Log.Errorf("failed to seed user : %s\n", err.Error())
		return err
	}
	seedElems := strings.Split(toSeed, ",")
	for el := range seedElems {
		switch seedElems[el] {
		case "genre":
			if err = MusicGenreSeed(cli); err != nil {
				Log.Errorf("failed to seed genre : %s\n", err.Error())
				return err
			}
		}
	}
	return nil
}
