.PHONY: polify-api

dbgen:
	stage=prod ./dbclientgen.sh

devmigrate:
	stage=dev ./migrate.sh

run-prod:
	docker run -p "8081:8081" -v "$$(pwd)/.prod.env:/go/.prod.env" -it registry.gitlab.com/polify/polify_api -stage=prod

run-preprod:
	docker run -p "8081:8081" -v "$$(pwd)/.preprod.env:/go/.preprod.env" -it registry.gitlab.com/polify/polify_api -stage=preprod

run-prod-arm:
	docker run -p "8081:8081" -v "$$(pwd)/.prod.env:/go/.prod.env" -it registry.gitlab.com/polify/polify_api:arm64 -stage=prod

run-preprod-arm:
	docker run -p "8081:8081" -v "$$(pwd)/.preprod.env:/go/.preprod.env" -it registry.gitlab.com/polify/polify_api:arm64 -stage=preprod

build-polify-api_docker:
	docker buildx build  --format docker --platform linux/arm64,linux/amd64 --manifest registry.gitlab.com/polify/polify_api .
build-polify-api_docker-arm:
	docker buildx build --build-arg TARGETPLATFORM=arm64 --format docker --platform linux/arm64 -t registry.gitlab.com/polify/polify_api:arm64 .
build-polify-api_docker-amd:
	docker buildx build --format docker --platform linux/amd64 -t registry.gitlab.com/polify/polify_api:amd64 .

push-polify-api_docker:
	docker push -f v2s2 registry.gitlab.com/polify/polify_api
push-polify-api_docker-arm:
	docker push registry.gitlab.com/polify/polify_api:arm64
push-polify-api_docker-amd:
	docker push registry.gitlab.com/polify/polify_api:amd64

docker-build: build-polify-api_docker

docker-build-arm: build-polify-api_docker-arm
docker-build-amd: build-polify-api_docker-amd


docker-push: push-polify-api_docker
docker-push-arm: push-polify-api_docker-arm
docker-push-amd: push-polify-api_docker-amd


