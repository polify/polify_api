package main

import (
	log "github.com/sirupsen/logrus"

	"github.com/getsentry/sentry-go"
	polify "gitlab.com/polify/polify_api"
)

func main() {
	if err := sentry.Init(sentry.ClientOptions{
		Dsn: "https://6414d1a1a446469e99f0d5fcc623c539@o1009285.ingest.sentry.io/6050687",
	}); err != nil {
		log.Error("sentry.Init: %s", err)
	}
	polify.Init(true)
}
