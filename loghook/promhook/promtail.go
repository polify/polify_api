package promhook

import (
	"fmt"
	"os"
	"time"

	// "github.com/afiskon/promtail-client/promtail"
	"github.com/ic2hrmk/promtail"
	"github.com/sirupsen/logrus"
)

var supportedLevels = []logrus.Level{logrus.DebugLevel, logrus.InfoLevel, logrus.WarnLevel, logrus.ErrorLevel}

// Config defines configuration for hook for Loki
type Config struct {
	URL                string
	LevelName          string
	Labels             map[string]string
	BatchWait          time.Duration
	BatchEntriesNumber int
}

func (c *Config) setDefault() {
	if c.LevelName == "" {
		c.LevelName = "severity"
	}
	if c.URL == "" {
		c.URL = "http://localhost:3100/api/prom/push"
	}
	if len(c.Labels) == 0 {
		c.Labels = map[string]string{
			"source": "test",
			"job":    "job",
		}
	}
	if c.BatchWait == 0 {
		c.BatchWait = 5 * time.Second
	}
	if c.BatchEntriesNumber == 0 {
		c.BatchEntriesNumber = 10000
	}

}

// Hook a logrus hook for loki
type Hook struct {
	clients map[logrus.Level]promtail.Client
}

// NewHook creates a new hook for Loki
func NewHook(c *Config) (*Hook, error) {
	var err error
	if c == nil {
		c = &Config{}
	}
	c.setDefault()
	// create different promtail client instance
	clients := make(map[logrus.Level]promtail.Client)
	for _, v := range supportedLevels {
		clients[v], err = promtail.NewJSONv1Client(c.URL, map[string]string{
			"severity":    v.String(),
			"application": "polify_api:v" + os.Getenv("APP_VERSION"),
			"stage":       os.Getenv("STAGE"),
		})
		if err != nil {
			return nil, fmt.Errorf("unable to init promtail client: %v", err)
		}
	}
	return &Hook{
		clients: clients,
	}, nil
}

// Fire implements interface for logrus
func (hook *Hook) Fire(entry *logrus.Entry) error {
	msg, err := entry.String()
	if err != nil {
		return err
	}
	switch entry.Level {
	case logrus.DebugLevel:
		hook.clients[entry.Level].Debugf(msg)
	case logrus.InfoLevel:
		hook.clients[entry.Level].Infof(msg)
	case logrus.WarnLevel:
		hook.clients[entry.Level].Warnf(msg)
	case logrus.ErrorLevel:
		hook.clients[entry.Level].Errorf(msg)
	default:
		return fmt.Errorf("unknown log level")
	}
	return nil
}

// Levels retruns supported levels
func (hook *Hook) Levels() []logrus.Level {
	return supportedLevels
}
