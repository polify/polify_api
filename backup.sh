#!/usr/bin/env bash
source ".$stage.env"
BACKUP_FOLDER=postgres_data
tmpstp=$(date '+%Y-%m-%d_%H:%M')
fpath="$BACKUP_FOLDER/dump_${DB_NAME}_${stage}_${tmpstp}.sql"
echo "[INFO] Will backup database in stage '$stage' in folder $BACKUP_FOLDER"
mkdir -p $BACKUP_FOLDER || true
pg_dump "$DB_NOOP" > $fpath
echo "[INFO] Done $fpath created"
