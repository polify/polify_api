-- @deprecated : Using "on the fly vector"
ALTER TABLE polify."Album" ADD IF NOT EXISTS "document_vectors" tsvector;
CREATE INDEX IF NOT EXISTS idx_fts_doc_vec_album ON polify."Album" USING gin(document_vectors);
UPDATE 
    polify."Album" 
SET 
    document_vectors = to_tsvector(name);