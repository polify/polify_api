-- @deprecated : Using "on the fly vector"
ALTER TABLE polify."Song" ADD IF NOT EXISTS "document_vectors" tsvector;
CREATE INDEX IF NOT EXISTS idx_fts_doc_vec ON polify."Song" USING gin(document_vectors);
UPDATE 
    polify."Song" 
SET 
    document_vectors = to_tsvector(title);