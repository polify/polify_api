-- @deprecated : Using "on the fly vector"
ALTER TABLE polify."Playlist" ADD IF NOT EXISTS "document_vectors" tsvector;
CREATE INDEX IF NOT EXISTS idx_fts_doc_vec_playlist ON polify."Playlist" USING gin(document_vectors);
UPDATE 
    polify."Playlist" 
SET 
    document_vectors = to_tsvector(name);