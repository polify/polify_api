-- @deprecated : Using "on the fly vector"
ALTER TABLE polify."Artist" ADD IF NOT EXISTS "document_vectors" tsvector;
CREATE INDEX IF NOT EXISTS idx_fts_doc_vec_artist ON polify."Artist" USING gin(document_vectors);
UPDATE 
    polify."Artist" 
SET 
    document_vectors = to_tsvector(name);