#!/usr/bin/env bash
source ".$stage.env"
export BUILDAH_FORMAT=docker
# echo "[INFO] Will deploy heroku app '$HEROKU_APP_NAME' environment with .$stage.env values"
# npx heroku container:login
# docker build -t "registry.heroku.com/$HEROKU_APP_NAME/web:latest" . &&\
#     docker push "registry.heroku.com/$HEROKU_APP_NAME/web:latest" &&\
#     npx heroku container:release web -a $HEROKU_APP_NAME
mv db/query-engine-debian-openssl-1.1.x_gen.go /tmp/query-engine-debian-openssl-1.1.x_gen.go
docker buildx build --platform=linux/arm64  -t registry.cloud.okteto.net/billotp/polify-api:latest .
mv /tmp/query-engine-debian-openssl-1.1.x_gen.go db/query-engine-debian-openssl-1.1.x_gen.go
mv /db/query-engine-linux-arm64-openssl-1.1.x_gen.go /tmp/query-engine-linux-arm64-openssl-1.1.x_gen.go
docker buildx build --platform=linux/amd64  -t registry.cloud.okteto.net/billotp/polify-api:latest .
mv /tmp/query-engine-linux-arm64-openssl-1.1.x_gen.go db/query-engine-linux-arm64-openssl-1.1.x_gen.go
docker push registry.cloud.okteto.net/billotp/polify-api:latest