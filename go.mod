module gitlab.com/polify/polify_api

go 1.16

require (
	github.com/99designs/gqlgen v0.14.0
	github.com/Pallinder/sillyname-go v0.0.0-20130730142914-97aeae9e6ba1
	github.com/agnivade/levenshtein v1.1.1 // indirect
	github.com/bogem/id3v2 v1.2.0
	github.com/cespare/xxhash v1.1.0
	github.com/cheggaaa/pb v1.0.29
	github.com/cloudinary/cloudinary-go v1.3.0
	github.com/creasty/defaults v1.5.2 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gabriel-vasile/mimetype v1.4.0
	github.com/getsentry/sentry-go v0.11.0
	github.com/gin-gonic/gin v1.7.4
	github.com/go-playground/validator/v10 v10.9.0 // indirect
	github.com/go-redis/redis/v8 v8.11.4
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/websocket v1.4.2
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/iancoleman/strcase v0.2.0
	github.com/ic2hrmk/promtail v0.0.4
	github.com/joho/godotenv v1.4.0
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/klauspost/cpuid/v2 v2.0.9 // indirect
	github.com/labstack/echo-contrib v0.12.0
	github.com/labstack/echo/v4 v4.6.1
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/mileusna/useragent v1.0.2
	github.com/minio/md5-simd v1.1.2 // indirect
	github.com/minio/minio-go/v7 v7.0.15
	github.com/minio/sha256-simd v1.0.0 // indirect
	github.com/mitchellh/mapstructure v1.4.2 // indirect
	github.com/prisma/prisma-client-go v0.12.2
	github.com/rs/xid v1.3.0 // indirect
	github.com/shopspring/decimal v1.3.1
	github.com/sirupsen/logrus v1.8.1
	github.com/takuoki/gocase v1.0.0
	github.com/tomasen/realip v0.0.0-20180522021738-f0c99a92ddce
	github.com/ugorji/go v1.2.6 // indirect
	github.com/vektah/gqlparser/v2 v2.2.0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	golang.org/x/net v0.0.0-20211020060615-d418f374d309 // indirect
	golang.org/x/sys v0.0.0-20220222200937-f2425489ef4c // indirect
	golang.org/x/tools v0.1.0 // indirect
	gopkg.in/ini.v1 v1.63.2 // indirect
)
