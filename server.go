package polify

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"runtime"
	"strings"
	"time"

	"gitlab.com/polify/polify_api/db"
	"gitlab.com/polify/polify_api/logrz"

	"gitlab.com/polify/polify_api/graph"
	"gitlab.com/polify/polify_api/graph/generated"
	"gitlab.com/polify/polify_api/middlewares"

	"github.com/labstack/echo-contrib/prometheus"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/extension"
	"github.com/99designs/gqlgen/graphql/handler/transport"
	"github.com/99designs/gqlgen/graphql/playground"

	"github.com/go-redis/redis/v8"
	"github.com/gorilla/websocket"
	"github.com/joho/godotenv"
)

type Cache struct {
	client *redis.Client
	ttl    time.Duration
}

const apqPrefix = "apq:"

var Log = logrz.LL

func NewCache(client *redis.Client, ttl time.Duration) (*Cache, error) {
	ctx := context.Background()
	err := client.Ping(ctx).Err()
	if err != nil {
		return nil, fmt.Errorf("could not create cache: %w", err)
	}

	return &Cache{client: client, ttl: ttl}, nil
}

func (c *Cache) Add(ctx context.Context, key string, value interface{}) {
	c.client.Set(ctx, apqPrefix+key, value, c.ttl)
}

func (c *Cache) Get(ctx context.Context, key string) (interface{}, bool) {
	s, err := c.client.Get(ctx, apqPrefix+key).Result()
	if err != nil {
		return struct{}{}, false
	}
	return s, true
}

// Defining the Graphql handler
func graphqlHandler(client *db.PrismaClient, rdb *redis.Client) echo.HandlerFunc {
	srv := handler.New(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{
		DB:    client,
		Cache: rdb,
	}}))
	srv.AddTransport(transport.Options{})
	srv.AddTransport(transport.GET{})
	srv.AddTransport(transport.POST{})
	srv.AddTransport(transport.MultipartForm{})
	srv.AddTransport(transport.Websocket{
		KeepAlivePingInterval: 10 * time.Second,
		Upgrader: websocket.Upgrader{
			CheckOrigin: func(r *http.Request) bool {
				fmt.Printf("[INFO] Subscription checkOrigin '%s'\n", r.Host)
				return true
			},
		},
		InitFunc: middlewares.InitFunc,
	})

	cache, err := NewCache(rdb, 10*time.Hour)
	if err != nil {
		logrz.LL.Fatalf("cannot create APQ redis cache: %v", err)
	}
	srv.Use(extension.AutomaticPersistedQuery{Cache: cache})
	srv.Use(extension.Introspection{})
	return func(c echo.Context) error {
		srv.ServeHTTP(c.Response().Writer, c.Request())
		return nil
	}
}

// Defining the Playground handler
func playgroundHandler() echo.HandlerFunc {
	h := playground.Handler("GraphQL", "/query")

	return func(c echo.Context) error {
		h.ServeHTTP(c.Response().Writer, c.Request())
		return nil
	}
}

func Init(shouldrun bool) (*echo.Echo, *db.PrismaClient) {
	var err error
	var stageFlag = flag.String("stage", "dev", "3 stages : dev, preprod (& hopefully prod someday)")

	flag.Parse()

	stVal := os.Getenv("STAGE")
	if stVal != "dev" && stVal != "" {
		stageFlag = &stVal
	}
	if err = os.Setenv("STAGE", *stageFlag); err != nil {
		panic(err)
	}
	// Loading .$stage.env if any
	envfName := "." + *stageFlag + ".env"
	if err = godotenv.Load(envfName); err != nil {
		Log.Infof("[ENV] No '%s' file to load, values should be in env\n", envfName)
	} else {
		Log.Infof("[ENV] File '%s' loaded\n", envfName)
	}
	logrz.Init()
	Log.Infof("[INIT] Starting exe '%s' on '%s' arch\n", os.Args[0], runtime.GOARCH)

	//
	// Should add postgresql db schema option
	// (dirty fix cause cannot modify env var in heroku)
	fixDbVal := os.Getenv("DATABASE_URL")
	if !strings.Contains(fixDbVal, "schema=polify") {
		if err = os.Setenv("DATABASE_URL", fixDbVal+"?schema=polify"); err != nil {
			panic(err)
		}
	}

	client := db.NewClient()
	if err = client.Prisma.Connect(); err != nil {
		Log.Fatal(err.Error())
	}
	defer func() {
		if err := client.Prisma.Disconnect(); err != nil {
			panic(err)
		}
	}()

	if err := MustSeed(client); err != nil {
		panic(err)
	}

	var redisUrl = os.Getenv("REDIS_URL")
	var redisTlsUrl = os.Getenv("REDIS_TLS_URL")
	if redisTlsUrl != "" {
		redisUrl = redisTlsUrl
	}
	opt, err := redis.ParseURL(redisUrl)
	if err != nil {
		panic(err)
	}
	if opt.TLSConfig != nil {
		opt.TLSConfig.InsecureSkipVerify = true
	}
	cache := redis.NewClient(opt)

	router := echo.New()

	// router.Use(middleware.Logger())
	router.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowMethods: []string{
			http.MethodPost,
			http.MethodOptions,
			http.MethodGet,
		},
		AllowHeaders: []string{
			"Authorization",
			"Content-Type",
		},
		AllowCredentials: true,
		AllowOrigins:     []string{"*"},
		MaxAge:           int(12 * time.Hour),
	}))

	router.Use(middlewares.SetAuthCookieEcho)
	router.Use(middlewares.SetContextEcho(client))
	router.GET("/", playgroundHandler())
	router.POST("/query", graphqlHandler(client, cache))
	port := os.Getenv("PORT")
	promPort := os.Getenv("PROM_PORT")
	if port == "" {
		port = "8081"
	}
	if promPort == "" {
		promPort = "8082"
	}
	router.HideBanner = true
	router.HidePort = true
	prom := prometheus.NewPrometheus("echo", nil)
	prom.SetPushGateway(os.Getenv("PROMETHEUS_ENDPOINT"), 3*time.Second)

	// Scrape metrics from Main Server
	prom.Use(router) // TODO -> Protect this endoint with a dedicated user
	// Setup metrics endpoint at another server

	if shouldrun {
		Log.Infof("[SERVER] Connect to http://127.0.0.1:%s for GraphQL playground 🚀\n", ":"+port)
		Log.Fatal(router.Start(":" + port))
	}
	return router, client
}

func Handler(w http.ResponseWriter, r *http.Request) {
	router, _ := Init(false)
	router.ServeHTTP(w, r) // ServeHTTP conforms to the http.Handler interface (https://godoc.org/github.com/gin-gonic/gin#Engine.ServeHTTP)
}
