#!/usr/bin/env bash
source ".$stage.env"
echo "[INFO] Will populate database schemas in stage '$stage' (will not generate client!)"
DATABASE_URL=$DATABASE_URL go run \
    github.com/prisma/prisma-client-go \
    db push --skip-generate
###  @deprecated : Using "on the fly vector"
# echo "[INFO] Creating song name ts vector"
# cat sql/songtitle_tsvector.sql | psql "$DB_NOOP"
# echo "[INFO] Creating album name ts vector"
# cat sql/albumname_tsvector.sql | psql "$DB_NOOP"
# echo "[INFO] Creating artists name ts vector"
# cat sql/artistname_tsvector.sql | psql "$DB_NOOP"
# echo "[INFO] Creating playlist name ts vector"
# cat sql/playlistname_tsvector.sql | psql "$DB_NOOP"