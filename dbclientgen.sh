#!/usr/bin/env bash
source ".$stage.env" || true
echo "[INFO] Will generate prisma db client in stage '$stage'"
go run \
    github.com/prisma/prisma-client-go \
    generate